package com.android.joy.Activities;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.joy.Adapters.SwipeAdapter;
import com.android.joy.Constants;
import com.android.joy.Helper.AddingFavouriteQuotesHelper;
import com.android.joy.Helper.AdmobAdHelper;
import com.android.joy.Helper.CustomDateForamtHelper;
import com.android.joy.R;
import com.android.joy.Room.User;
import com.android.joy.Room.UserDatabase;
import com.android.joy.util.NetworkConnection;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EveningQuotes extends AppCompatActivity
{

    AdmobAdHelper admobAdHelper;

    boolean userCanDisplayAd = false;

    boolean isOnLastPage = false;

    int timerSeconds;

    boolean userWatchedTheAd = false;

    public static UserDatabase userDatabase;

    ViewPager viewPagerEvening;

    SwipeAdapter swipeAdapter;

    TextView textEveningFragment,
            textLikeEveningFragment,
            textSavedEveningFragment,
            textShareEveningFragment,
            uselessTextViewEveningFragment,
            textNotificationEveningFragment;

    ImageView iconNotificationEveningFragment, favEvening;

    Spinner dayOfEveningFragment,
            timeOfEveningFragment;

    int[] slider_images = {R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box};

    DatabaseReference mDatabase;

    ArrayList<String> quoteData = new ArrayList<>();
    ArrayList<String> quoteOwner = new ArrayList<>();

    String[] quotes;
    String[] owners;

    ProgressBar eveningProgress;

    String childOfEveningDatabase;
    TextView noInternetTextView;
    TextView eveningDate;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_evening_quotes);

        registerViews();

        hideUnusedThings();

        createAndSetToolBar();

        initializeAndSetAdapter();

        initializeSpinners(dayOfEveningFragment, timeOfEveningFragment);

        registerListenersNew();

        trackViewPagerListener();

        textShareEveningFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = viewPagerEvening.getCurrentItem();
                String text = quotes[i];
                shareQuote(text);
            }
        });
    }

    private void trackViewPagerListener()
    {
        viewPagerEvening.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int i, float v, int i1)
            {

            }

            @Override
            public void onPageSelected(int i)
            {
                if (i == 2)
                {
                    // start a timer here to check, if 5 second gets passes then it means we have to show the add
                    isOnLastPage = true;
                    Thread t = createTimerThread();
                    t.start();

                }
                else
                {
                    isOnLastPage = false;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i)
            {

                int item = viewPagerEvening.getCurrentItem();
                String text = quotes[item];
                if (checkIfDataAlreadyExists(text))
                {
                    favEvening.setTag("heart");
                    favEvening.setImageResource(R.drawable.heart);
                }
                else
                {
                    favEvening.setTag("like");
                    favEvening.setImageResource(R.drawable.like_icon);
                }
            }
        });
    }

    private void registerListenersNew()
    {
        favEvening.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String text = "";
                int item = viewPagerEvening.getCurrentItem();
                text = quotes[item];

                String owner = owners[item];

                if (NetworkConnection.isConnectingToInternet(EveningQuotes.this))
                {
                    if (checkIfDataAlreadyExists(text)) {
                        // already exists
                        Toast.makeText(getApplicationContext(), "Already Added to favourite quotes", Toast.LENGTH_SHORT).show();
                    } else {
                        eveningProgress.setVisibility(View.VISIBLE);
                        Toast.makeText(EveningQuotes.this, "Please wait for the quote to be added", Toast.LENGTH_SHORT).show();
                        addToFav(text, owner);
                        eveningProgress.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), "Added to favourite quotes", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "You are not connected to internet !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addToFav(String text, String owner)
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        AddingFavouriteQuotesHelper object = new AddingFavouriteQuotesHelper(EveningQuotes.this, user.getUid().toString(), user.getDisplayName().toString(), text, favEvening, owner);
        object.addQuoteToFavourite();
    }

    private boolean checkIfDataAlreadyExists(String text)
    {
        List<User> users = EveningQuotes.userDatabase.daoAccess().getUsers();

            boolean hasValue = false;

            for (int i = 0; i < users.size(); i++)
            {
                User user = users.get(i);
                if (user.getQuotes().equals(text))
                {
                    hasValue = true;
                    break;
                }
            }
            if (hasValue)
            {
                return true;
            } else
                {
                    return false;
                }
    }

    private void createAndSetToolBar()
    {
        Toolbar toolbar = findViewById(R.id.toolbarEvening);
        setSupportActionBar(toolbar);
        toolbar.setTitle(Constants.MORNING_QUOTE);
        toolbar.setNavigationIcon(Constants.BACK_ARROW);
        pressBackButton(toolbar); // Handling on back button pressed
        toolbar.setTitleTextColor(getResources().getColor(Constants.DOTS_COLOR_WHITE));
    }

    private void registerViews()
    {
        quotes = new String[3];
        owners = new String[3];

        textEveningFragment = findViewById(R.id.textEveningFragmentDay);

        textLikeEveningFragment = findViewById(R.id.textLikesEveningFragment);

        textSavedEveningFragment = findViewById(R.id.textSavedEveningFragment);

        textShareEveningFragment = findViewById(R.id.textShareEveningFragment);

        uselessTextViewEveningFragment = findViewById(R.id.uselessTextViewEveningFragment);

        textNotificationEveningFragment = findViewById(R.id.textNotificationEveningFragment);

        iconNotificationEveningFragment = findViewById(R.id.iconNotificationEveningFragment);

        favEvening = findViewById(R.id.favImageEvening);

        dayOfEveningFragment = findViewById(R.id.spinnerDayEveningFragment);

        timeOfEveningFragment = findViewById(R.id.spinnerTimeEveningFragment);

        viewPagerEvening = findViewById(R.id.viewPagerFragmentEvening);

        eveningProgress = findViewById(R.id.evening_progress);

        noInternetTextView = findViewById(R.id.textViewNoInternetEvening);

        Calendar calendar = Calendar.getInstance();

        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        if (hour < 18)
        {
            favEvening.setEnabled(false);
            textShareEveningFragment.setEnabled(false);
            userCanDisplayAd = false;
        }
        else
        {
            favEvening.setEnabled(true);
            textShareEveningFragment.setEnabled(true);
            userCanDisplayAd = true;
        }

        favEvening.setEnabled(false);

        textShareEveningFragment.setEnabled(false);

        userDatabase = Room.databaseBuilder(EveningQuotes.this, UserDatabase.class, Constants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        userCanDisplayAd = false;

        admobAdHelper = new AdmobAdHelper(EveningQuotes.this, getResources().getString(R.string.test_google_app_id), getResources().getString(R.string.test_google_ad_unit_id), getResources().getString(R.string.client_test_device_id));

        eveningDate = findViewById(R.id.date_evening);

        updateTextYesterday();

    }

    private void updateTextYesterday()
    {
        int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

        if (hours >= 0 && hours < 6)
        {
            textEveningFragment.setText("Yesterday's");
            eveningDate.setText(CustomDateForamtHelper.getInstance().getPreviousDate());
        }
        else
        {
            eveningDate.setText(CustomDateForamtHelper.getInstance().getTodaysDate());
        }
    }

    private void initializeAndSetAdapter()
    {
        initAndSetEmptyAdapter();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Quotes").child("Categories").child("Evening");

        if (NetworkConnection.isConnectingToInternet(this)) // it means internet is connecting
        {

            int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (hour >= 18 || (hour >=0 && hour < 6))
            {
                Toast.makeText(this, "Please wait while the data is been loaded !", Toast.LENGTH_SHORT).show();
            }

            childOfEveningDatabase = CustomDateForamtHelper.getInstance().getTodaysDate();

            final String currentPeriod = CustomDateForamtHelper.getInstance().returnPeriodOfTime();

            final boolean[] hasChild = {false};

            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(childOfEveningDatabase))
                    {
                        hasChild[0] = true;
                    }
                    if (hasChild[0])
                    {
                        displayQuotes(mDatabase, currentPeriod);
                    }
                    else
                    {
                        showThereAreNoQuotes();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
        else
        {
            quoteData.add("");
            quoteData.add("");
            quoteData.add("");

            quoteOwner.add("");
            quoteOwner.add("");
            quoteOwner.add("");

            placeAlertTextView();
            removeProgressDialog();
            setAdapterAfterGettingData();
        }
    }

    private void displayQuotes(DatabaseReference mDatabase, String currentPeriod)
    {
        if (checkForEquals(currentPeriod, "evening"))
        {

            mDatabase.child(childOfEveningDatabase).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.hasChild("text") && snapshot.hasChild("owner")) {
                            String quoteText = snapshot.child("text").getValue().toString();
                            String quoteQwner = snapshot.child("owner").getValue().toString();
                            quoteData.add(quoteText);
                            quoteOwner.add(quoteQwner);
                        }
                        //quoteData.add(snapshot.getValue().toString());
                    }
                    removeProgressDialog();
                    setAdapterAfterGettingData();
                    textShareEveningFragment.setEnabled(true);
                    userCanDisplayAd = true;
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else if (checkForEquals(currentPeriod, "anotherday")) {
            mDatabase.child(CustomDateForamtHelper.getInstance().returnDate("anotherday")).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.exists()) {
                            if (snapshot.hasChild("text") && snapshot.hasChild("owner")) {
                                String quoteText = snapshot.child("text").getValue().toString();
                                String quoteQwner = snapshot.child("owner").getValue().toString();
                                quoteData.add(quoteText);
                                quoteOwner.add(quoteQwner);
                            }
                            //quoteData.add(snapshot.getValue().toString());
                        }
                    }
                    removeProgressDialog();
                    setAdapterAfterGettingData();
                    textShareEveningFragment.setEnabled(true);
                    userCanDisplayAd = true;
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            quoteData.add("");
            quoteData.add("");
            quoteData.add("");

            quoteOwner.add("");
            quoteOwner.add("");
            quoteOwner.add("");

            removeProgressDialog();
            noInternetTextView.setVisibility(View.VISIBLE);
            noInternetTextView.setText("Please wait for the quotes");
            setAdapterAfterGettingData();
        }
    }

    private void showThereAreNoQuotes()
    {
        noInternetTextView.setText("Sorry, there are no quotes for today");
        noInternetTextView.setVisibility(View.VISIBLE);
        removeProgressDialog();
    }

    private void placeAlertTextView()
    {
        eveningProgress.setVisibility(View.INVISIBLE);
        noInternetTextView.setVisibility(View.VISIBLE);
    }

    private void setAdapterAfterGettingData()
    {
        quotes[0] = quoteData.get(0);
        quotes[1] = quoteData.get(1);
        quotes[2] = quoteData.get(2);

        owners[0] = quoteOwner.get(0);
        owners[1] = quoteOwner.get(1);
        owners[2] = quoteOwner.get(2);

        checkFirstEntryWhenActivityOpens();

        swipeAdapter = new SwipeAdapter(this, slider_images, quotes, owners);
        viewPagerEvening.setAdapter(swipeAdapter);

        Calendar calendar = Calendar.getInstance();

        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        if (hour >= 18)
        {
            favEvening.setEnabled(true);
        }
    }

    private void pressBackButton(Toolbar toolbar)
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeSpinners(Spinner daysSpinner, Spinner timeSpinner)
    {
        ArrayAdapter<CharSequence> dayAdapter = ArrayAdapter.createFromResource(this, R.array.days, android.R.layout.simple_spinner_dropdown_item);
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daysSpinner.setAdapter(dayAdapter);

        ArrayAdapter<CharSequence> timeAdapter = ArrayAdapter.createFromResource(this, R.array.times, android.R.layout.simple_spinner_dropdown_item);
        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(timeAdapter);
    }

    private void initQuotes()
    {
        quotes[0] = quotes[1] = quotes[2] = "";
        owners[0] = owners[1] = owners[2] = "";
    }

    private void initAndSetEmptyAdapter()
    {
        initQuotes();

        swipeAdapter = new SwipeAdapter(this, slider_images, quotes, owners);
        viewPagerEvening.setAdapter(swipeAdapter);
    }

    private void removeProgressDialog()
    {
        eveningProgress.setVisibility(View.INVISIBLE);
    }

    private boolean checkForEquals(String a, String b)
    {
        if (a.equals(b))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void hideUnusedThings()
    {
        textLikeEveningFragment.setVisibility(View.INVISIBLE);

        textSavedEveningFragment.setVisibility(View.INVISIBLE);

        uselessTextViewEveningFragment.setVisibility(View.INVISIBLE);

        textNotificationEveningFragment.setVisibility(View.INVISIBLE);

        iconNotificationEveningFragment.setVisibility(View.INVISIBLE);

        dayOfEveningFragment.setVisibility(View.INVISIBLE);

        dayOfEveningFragment.setEnabled(false);

        timeOfEveningFragment.setVisibility(View.INVISIBLE);

        timeOfEveningFragment.setEnabled(false);
    }

    private void checkFirstEntryWhenActivityOpens()
    {
        String text = quotes[0];
        if (checkIfDataAlreadyExists(text))
        {
            favEvening.setTag("heart");
            favEvening.setImageResource(R.drawable.heart);
        }
        else
        {
            favEvening.setTag("like");
            favEvening.setImageResource(R.drawable.like_icon);
        }
    }

    private void shareQuote(String text)
    {
        if (NetworkConnection.isConnectingToInternet(this))
        {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject/Title");
            //intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.putExtra(Intent.EXTRA_TEXT, text + "\nDownload Joy for more quotes\n" + "https://www.google.com");
            startActivity(Intent.createChooser(intent, "Choose Sharing Method"));
        }
    }

    private Thread createTimerThread()
    {
        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                timerSeconds = Calendar.getInstance().get(Calendar.SECOND);
                final int minute = Calendar.getInstance().get(Calendar.MINUTE);

                while(isOnLastPage) {
                    if (userCanDisplayAd) {

                        if (!userWatchedTheAd) {
                            if (isOnLastPage) {
                                try {
                                    Thread.sleep(1000);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            int currSecond = Calendar.getInstance().get(Calendar.SECOND);
                                            int currMinute = Calendar.getInstance().get(Calendar.MINUTE);
                                            if (currMinute - minute == 1) {
                                                currSecond -= 60;
                                                if (currSecond - timerSeconds >= 5) {
                                                    admobAdHelper.displayAd();
                                                    userWatchedTheAd = true;
                                                }
                                            } else if (currMinute - minute == 0) {
                                                if (currSecond - timerSeconds >= 5) {
                                                    admobAdHelper.displayAd();
                                                    userWatchedTheAd = true;
                                                }
                                            }
                                        }
                                    });
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        };
        return t;
    }

}
