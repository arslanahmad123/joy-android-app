package com.android.joy.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.joy.Constants;
import com.android.joy.Helper.Facebookuser;
import com.android.joy.R;
import com.android.joy.Room.User;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class CategoriesScreenActivity
        extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{

    ImageView[] imageViews;

    ImageView profilePic;

    TextView[] textViews;

    TextView nameOfPerson;

    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_categories_screen);

        createComponents();

        createViewsArrays();

        registerViews();

        putUserData();

        registerListeners();

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        nameOfPerson.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else { super.onBackPressed(); }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //getMenuInflater().inflate(R.menu.categories_screen, menu); the settings menu is been removed
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        // if (id == R.id.action_settings) { return true; } the action_settings is removed

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.mor_quot)
        {
            startActivity(new Intent(CategoriesScreenActivity.this, MorningQuotes.class));
        }
        else if (id == R.id.aft_quot)
        {
            startActivity(new Intent(CategoriesScreenActivity.this, AfternoonQuotes.class));
        }
        else if (id == R.id.eve_quot)
        {
            startActivity(new Intent(CategoriesScreenActivity.this, EveningQuotes.class));
        }
        else if (id == R.id.fav_quot)
        {
            startActivity(new Intent(CategoriesScreenActivity.this, FavouriteQuotes.class));
        }
        else if (id == R.id.settings)
        {
            startActivity(new Intent(CategoriesScreenActivity.this, SettingsActivity.class));
        }
        else if (id == R.id.logout)
        {
            askTheUser();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void askTheUser()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Constants.LOGOUT_MESSAGE);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                Intent intent = new Intent(CategoriesScreenActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                closeDialog(alertDialog);

                intent.putExtra("message", "This action is from Category Screen");

                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();

                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

                GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(CategoriesScreenActivity.this, gso);

                if (currentUser != null)
                {
                    LoginManager.getInstance().logOut();
                    mAuth.signOut();
                    mGoogleSignInClient.signOut().
                            addOnCompleteListener(
                            CategoriesScreenActivity.this,
                            new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.d("Success", "Google Sign out successfull");
                        }
                    });

                }

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(Constants.SHARED_PREFERENCES_KEY_USER_INFO);
                editor.clear();
                editor.commit();

                clearAllFavQuotesOfUser();

                startActivity(intent);
                finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                closeDialog(alertDialog);
            }
        });

        alertDialog = builder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                // setting text size
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(14);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(14);

                // setting text color
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.customNotificationBar));
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.customNotificationBar));
            }
        });

        alertDialog.show();
    }

    private void clearAllFavQuotesOfUser()
    {
        List<User> users1 = MorningQuotes.userDatabase.daoAccess().getAll();

        for (int j=0; j<users1.size(); j++)
        {
            MorningQuotes.userDatabase.daoAccess().delete(users1.get(j));
        }

    }

    private void closeDialog(AlertDialog dialog)
    {
        dialog.cancel();
    }

    private void createComponents()
    {
        Toolbar toolBar = createToolBar();

        setToolBar(toolBar);

        DrawerLayout drawer = createDrawer();

        ActionBarDrawerToggle drawerToggle = createActionDrawerToggle(drawer, toolBar);

        addDrawerListener(drawerToggle, drawer);

        createNavigationView();
    }

    private ActionBarDrawerToggle createActionDrawerToggle(DrawerLayout drawer, Toolbar toolbar)
    {
        ActionBarDrawerToggle toggle
                = new ActionBarDrawerToggle (this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        return toggle;
    }

    private DrawerLayout createDrawer()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        return drawer;
    }

    private void setToolBar(Toolbar toolBar)
    {
        setSupportActionBar(toolBar);
        toolBar.setTitle("Categories");
    }

    private Toolbar createToolBar()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        return toolbar;
    }

    private void createNavigationView()
    {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void createViewsArrays()
    {
        imageViews = new ImageView[3];
        textViews = new TextView[3];
    }

    private void registerViews()
    {
        imageViews[0] = findViewById(R.id.category_morning_image);
        imageViews[1] = findViewById(R.id.category_afternoon_image);
        imageViews[2] = findViewById(R.id.category_evening_image);

        textViews[0] = findViewById(R.id.textMorningQuote);
        textViews[1] = findViewById(R.id.textAfternoonQuote);
        textViews[2] = findViewById(R.id.textEveningQuote);

    }

    private void registerListeners()
    {
        imageViews[0].setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CategoriesScreenActivity.this, MorningQuotes.class);
                startActivity(intent);
            }
        });

        imageViews[1].setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CategoriesScreenActivity.this, AfternoonQuotes.class);
                startActivity(intent);
            }
        });

        imageViews[2].setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CategoriesScreenActivity.this, EveningQuotes.class);
                startActivity(intent);
            }
        });

    }

    private void addDrawerListener(ActionBarDrawerToggle toggle, DrawerLayout drawer)
    {
        addDrawerListenerToDrawer(toggle, drawer);
    }

    private void addDrawerListenerToDrawer(ActionBarDrawerToggle toggle, DrawerLayout drawer)
    {
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void putUserData()
    {
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
        //String url = intent.getStringExtra("uri");
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String user = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_USER_INFO, "");
        Facebookuser fbUser = gson.fromJson(user, Facebookuser.class);
        String url = fbUser.getImageUri().toString();
        String accType = intent.getStringExtra("accType");

        Bundle extras = intent.getExtras();

        Uri photoUrl = null;

        if (extras != null && url != null)
        {
            photoUrl = Uri.parse(url);
        }

        Log.d("Yes1",  name + ", " + email + ", " + url);

        View header = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);

        nameOfPerson = header.findViewById(R.id.nameOfPerson);
        profilePic = header.findViewById(R.id.profilePic);

        nameOfPerson.setText(name);

        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.TRANSPARENT)
                .borderWidthDp(0)
                .cornerRadiusDp(50)
                .oval(false)
                .build();

        Picasso.with(this).load(photoUrl).fit().transform(transformation).into(profilePic);

    }


}
