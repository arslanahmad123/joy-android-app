package com.android.joy.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.joy.Adapters.SwipeAdapter;
import com.android.joy.Constants;
import com.android.joy.Helper.CustomDateForamtHelper;
import com.android.joy.Helper.FirebaseData;
import com.android.joy.R;
import com.android.joy.util.NetworkConnection;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class FavouriteQuotes extends AppCompatActivity
{

    DatabaseReference mDatabaseReference;

    ViewPager viewPagerFavourite;

    SwipeAdapter swipeAdapter;

    ProgressBar favouriteProgress;

    TextView noInternetTextView;

    TextView todayText, todayDate;

    int[] slider_images =
            {R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box,
            R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box,
            R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box,
            R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box,
            R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box,
            R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box,
            R.drawable.quote_box, R.drawable.quote_box};

    DatabaseReference childReference;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_quotes);
        registerViews();
        displayLoaderProgress();
        createAndSetToolBar();

        setDefaultAdapter();

        if (NetworkConnection.isConnectingToInternet(this))
        {
            setDataToAdapter(FirebaseAuth.getInstance().getCurrentUser().getUid());
        }
        else
        {
            hideLoaderProgress();
            showThereisNoInternet();
        }
    }

    private void setDefaultAdapter()
    {
        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) >= 0 && Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < 6)
        {
            todayDate.setText(CustomDateForamtHelper.getInstance().getPreviousDate());
        }
        else
        {
            todayDate.setText(CustomDateForamtHelper.getInstance().getTodaysDate());
        }
        int[] images = {slider_images[0]};
        String[] quotes = {""};
        String[] owners = {""};
        swipeAdapter = new SwipeAdapter(this, images, quotes, owners);
        viewPagerFavourite.setAdapter(swipeAdapter);
    }

    private void displayLoaderProgress()
    {
        favouriteProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoaderProgress()
    {
        favouriteProgress.setVisibility(View.INVISIBLE);
    }

    private void showThereisNoInternet()
    {
        noInternetTextView.setText("Sorry, there is not internet");
        noInternetTextView.setVisibility(View.VISIBLE);
        hideLoaderProgress();
    }

    private void showThereisNoFavouriteQuote()
    {
        noInternetTextView.setText("Sorry, you don't have any favourite quotes");
        noInternetTextView.setVisibility(View.VISIBLE);
        hideLoaderProgress();
    }

    private void registerViews()
    {
        viewPagerFavourite = findViewById(R.id.viewPagerFragmentFavourite);
        favouriteProgress = findViewById(R.id.favourite_progress);
        noInternetTextView = findViewById(R.id.textViewNoInternetFavourite);
        todayText = findViewById(R.id.todayTextFav);
        todayDate = findViewById(R.id.date_fav);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        updateTextTomorrow();
    }

    private void updateTextTomorrow()
    {
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

        if (hour >= 0 && hour < 6)
        {
            todayText.setText("Tomorrow's");
        }
    }

    private void createAndSetToolBar()
    {
        Toolbar toolbar = findViewById(R.id.toolbarFavourite);
        setSupportActionBar(toolbar);
        toolbar.setTitle(Constants.MORNING_QUOTE);
        toolbar.setNavigationIcon(Constants.BACK_ARROW);
        pressBackButton(toolbar); // Handling on back button pressed
        toolbar.setTitleTextColor(getResources().getColor(Constants.DOTS_COLOR_WHITE));
    }

    private void pressBackButton(Toolbar toolbar)
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FavouriteQuotes.this, CategoriesScreenActivity.class));
                finish();
            }
        });
    }

    private void setDataToAdapter(final String uid) // correct function order ..
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Users");

        if (reference != null)
        {
            reference.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot snapshot: dataSnapshot.getChildren())
                    {
                        String id = snapshot.child("id").getValue().toString();

                        if (id.equals(uid))
                        {
                            Log.d("Yes", "Same");
                            gettingRefrence(snapshot.getRef());
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });

        }

    }

    private void gettingRefrence(final DatabaseReference ref)
    {

        ref.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.hasChild("fav"))
                {
                    for (DataSnapshot snapshot: dataSnapshot.child("fav").getChildren())
                    {
                        settingData(snapshot.getRef());
                        break;
                    }
                }
                else
                {
                    showThereisNoFavouriteQuote();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });

    }

    private void settingData(DatabaseReference ref)
    {
        // this 'ref' is the refrence of the child of fav quote of the specific user
        if (ref != null) // means there are some quotes, atleast 1
        {

            ref.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {

                    DatabaseReference childRefrence = dataSnapshot.child("Quote").getRef();

                    if (childRefrence != null)
                    {
                        int childCount = (int) dataSnapshot.child("Quote").getChildrenCount();

                        if (dataSnapshot.child("Quote").getChildrenCount() != 0)
                        {

                            ArrayList<String> quotes = new ArrayList<>();
                            ArrayList<String> owners = new ArrayList<>();

                            // means there are more than one quotes present
                            for (DataSnapshot snapshot: dataSnapshot.child("Quote").getChildren())
                            {
                                if (snapshot.hasChild("text") && snapshot.hasChild("owner"))
                                {
                                    String quote = snapshot.child("text").getValue().toString();
                                    String owner = snapshot.child("owner").getValue().toString();

                                    quotes.add(quote);
                                    owners.add(owner);
                                }
                            }

                            int[] images = new int[quotes.size()];

                            for (int i=0; i<quotes.size(); i++)
                            {
                                images[i] = slider_images[i];
                            }

                            String[] quotesData = new String[quotes.size()];
                            String[] quotesOwners = new String[owners.size()];

                            for (int i=0; i<quotes.size(); i++)
                            {
                                quotesData[i] = quotes.get(i);
                                quotesOwners[i] = owners.get(i);
                            }

                            swipeAdapter = new SwipeAdapter(FavouriteQuotes.this, images, quotesData, quotesOwners);
                            hideLoaderProgress();
                            viewPagerFavourite.setAdapter(swipeAdapter);

                        }
                        else
                        {
                            // means there are only one quote present
                            String[] quotes = {dataSnapshot.child("text").getValue().toString()};
                            String[] owners = {dataSnapshot.child("owner").getValue().toString()};

                            int[] images = {slider_images[0]};

                            swipeAdapter = new SwipeAdapter(FavouriteQuotes.this, images, quotes, owners);
                            hideLoaderProgress();
                            viewPagerFavourite.setAdapter(swipeAdapter);
                        }
                    }
                    else
                    {
                        showThereisNoFavouriteQuote();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });

        }
        else
        {
            // means no favourite quote
            showThereisNoFavouriteQuote();
        }
    }


}
