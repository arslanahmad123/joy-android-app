package com.android.joy.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.joy.Constants;
import com.android.joy.Fragments.NetworkFragment;
import com.android.joy.Helper.AddingFavouriteQuotesHelper;
import com.android.joy.Helper.FacebookLoginHelper;
import com.android.joy.Helper.Facebookuser;
import com.android.joy.Helper.FirebaseData;
import com.android.joy.Helper.FirebaseLoginHelper;
import com.android.joy.Helper.GoogleLoginHelper;
import com.android.joy.R;
import com.android.joy.Room.User;
import com.android.joy.Room.UserDatabase;
import com.android.joy.util.NetworkConnection;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    // attributes for

    public static UserDatabase userDatabase;

    // facebook login button
    private LoginButton mLoginButton;
    // google sign in button
    private SignInButton mSignInButton;

    private ImageView imageViewLogin;
    private TextView textViewLogin;

    private ImageView custom_fb_btn, custom_gmail_btn;

    private ProgressBar progressBarLogin;

    // Firebase
    private FirebaseAuth mFirebaseAuth;

    // Facebook
    private AccessToken mAccessToken;
    private CallbackManager mCallBackManager;

    // Google
    private GoogleSignInOptions mGoogleSignInOptions;
    private GoogleSignInClient mGoogleSignInClient;

    // Helper Objects
    FirebaseLoginHelper mFirebaseLoginHelper;
    FacebookLoginHelper mFacebookLoginHelper;
    GoogleLoginHelper mGoogleLoginHelper;

    // Helper Object Variable
    Facebookuser facebookuser;

    @Override
    protected void onStart() {
        super.onStart();

        Context context = getApplicationContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        boolean hasKey = sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_USER_INFO);
        if (hasKey) {
            String userInfoJsonString = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_USER_INFO, "");
            Gson gson = new Gson();
            Facebookuser fUser = gson.fromJson(userInfoJsonString, Facebookuser.class);
            showLoadingScreen();
            moveToNextActivity(sharedPreferences, fUser);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        registerViews();

        createHelperObjects();

        registerListeners();
    }

    private void registerListeners() {
        mLoginButton.setOnClickListener(this);
        mSignInButton.setOnClickListener(this);

        custom_fb_btn.setOnClickListener(this);
        custom_gmail_btn.setOnClickListener(this);

    }

    private void registerViews() {
        mLoginButton = findViewById(R.id.login_button);

        mSignInButton = findViewById(R.id.sign_in_button);

        imageViewLogin = findViewById(R.id.imageViewLogin);

        textViewLogin = findViewById(R.id.textViewLogin);

        custom_fb_btn = findViewById(R.id.custom_fb);

        custom_gmail_btn = findViewById(R.id.custom_gmail);

        progressBarLogin = findViewById(R.id.progress_bar_login);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mAccessToken = AccessToken.getCurrentAccessToken();

        mCallBackManager = CallbackManager.Factory.create();

        mGoogleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Constants.AUTH_CLIENT_KEY)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(
                this,
                mGoogleSignInOptions);

        userDatabase = Room.databaseBuilder(LoginActivity.this, UserDatabase.class, Constants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
    }

    private void createHelperObjects() {
        mFirebaseLoginHelper = new FirebaseLoginHelper(mFirebaseAuth);
        mFacebookLoginHelper = new FacebookLoginHelper(mAccessToken, mCallBackManager);
        mGoogleLoginHelper = new GoogleLoginHelper(mGoogleSignInOptions, mGoogleSignInClient);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.custom_fb:
                mLoginButton.performClick();
                registerLoginButtonCallback();
                break;
            case R.id.custom_gmail:
                mSignInButton.performClick();
                signInWithGoogle();
                break;
        }

    }

    private Facebookuser getUserProfile(FirebaseUser currentUser, String accType) {
        if (currentUser != null) {
            String name = currentUser.getDisplayName();
            String email = currentUser.getEmail();
            Uri url = currentUser.getPhotoUrl();
            return new Facebookuser(name, url.toString(), email, accType);
        } else {
            return null;
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        final ArrayList<Facebookuser> users = new ArrayList<>();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(
                        this,
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    final FirebaseUser user = mFirebaseAuth.getCurrentUser();
                                    final boolean[] hasChild = {false};
                                    DatabaseReference childExists = FirebaseDatabase.getInstance().getReference().child("Users");
                                    childExists.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                if (snapshot.child("id").getValue().toString().equals(user.getUid()))
                                                {
                                                    hasChild[0] = true;
                                                    break;
                                                }
                                            }
                                            if (!hasChild[0])
                                            {
                                                setNewUserToFirebaseDatabase(user.getUid(), user.getDisplayName());
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                    facebookuser = getUserProfile(user, "facebook");
                                    showLoadingScreen();
                                    moveToNextActivity(facebookuser);
                                    Log.d("Firebase", user.getUid() + " " + user.getEmail());
                                } else {
                                    Log.d("Firebase", "Authentication failed");
                                }
                            }
                        });
    }

    private void registerLoginButtonCallback() {
        mLoginButton.registerCallback(
                mCallBackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        mAccessToken = loginResult.getAccessToken();
                        handleFacebookAccessToken(mAccessToken);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException error) {
                    }
                });
    }

    private void signInWithGoogle() {
        Intent signIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signIntent, Constants.RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
            }
        }

        mCallBackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        //Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(
                        this,
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    //Log.d(TAG, "signInWithCredential:success");
                                    final FirebaseUser user = mFirebaseAuth.getCurrentUser();
                                    final boolean[] hasChild = {false};
                                    DatabaseReference childExists = FirebaseDatabase.getInstance().getReference().child("Users");
                                    childExists.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                if (snapshot.child("id").getValue().toString().equals(user.getUid()))
                                                {
                                                    hasChild[0] = true;
                                                    break;
                                                }
                                            }
                                            if (!hasChild[0])
                                            {
                                                setNewUserToFirebaseDatabase(user.getUid(), user.getDisplayName());
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    Facebookuser fUser = getUserProfile(user, "google");

                                    showLoadingScreen();
                                    moveToNextActivity(fUser);

                                } else {
                                    //Log.w(TAG, "signInWithCredential:failure", task.getException());
                                }

                            }
                        });
    }

    private void moveToNextActivity(Facebookuser user) {
        Intent intent = new Intent(LoginActivity.this, CategoriesScreenActivity.class);
        intent.putExtra("name", user.getName());
        intent.putExtra("email", user.getEmail());
        intent.putExtra("uri", user.getImageUri().toString());
        storeUserProfileToSharedPref(user);

        FirebaseUser currUser = FirebaseAuth.getInstance().getCurrentUser();

        AddingFavouriteQuotesHelper object = new AddingFavouriteQuotesHelper(LoginActivity.this, currUser.getUid().toString(), currUser.getDisplayName().toString(), "", null, "");

        object.storeFavouriteQuotesToLocalStorage();

        startActivity(intent);
        finish();
    }

    private void moveToNextActivity(SharedPreferences sharedPreferences, Facebookuser facebookuser) {
        Intent intent = new Intent(LoginActivity.this, CategoriesScreenActivity.class);
        intent.putExtra("name", facebookuser.getName());
        intent.putExtra("email", facebookuser.getEmail());
        Uri uri = Uri.parse(facebookuser.getImageUri());
        intent.putExtra("url", uri.toString());
        intent.putExtra("accType", facebookuser.getAccountType());

        FirebaseUser currUser = FirebaseAuth.getInstance().getCurrentUser();

        AddingFavouriteQuotesHelper object = new AddingFavouriteQuotesHelper(LoginActivity.this, currUser.getUid().toString(), currUser.getDisplayName().toString(), "", null, "");


        // call method to populate data of user when users logs in
        object.storeFavouriteQuotesToLocalStorage();

        startActivity(intent);
        finish();
    }

    private void storeUserProfileToSharedPref(Facebookuser facebookuser) {
        Gson gson = new Gson();
        String userInfo = gson.toJson(facebookuser);
        Context context = getApplicationContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SHARED_PREFERENCES_KEY_USER_INFO, userInfo);
        editor.commit();
    }

    private void makeViewsInvisible() {
        imageViewLogin.setVisibility(View.INVISIBLE);
        textViewLogin.setVisibility(View.INVISIBLE);
        custom_fb_btn.setVisibility(View.INVISIBLE);
        custom_gmail_btn.setVisibility(View.INVISIBLE);
    }

    private void showLoginProgressSpinner() {
        progressBarLogin.setVisibility(View.VISIBLE);
    }

    private void showLoadingScreen() {
        //makeViewsInvisible();
        showLoginProgressSpinner();
    }

    private void setNewUserToFirebaseDatabase(String tokenId, String name)
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        Map<String, Object> userData = new HashMap<>();
        userData.put("id", tokenId);
        userData.put("name", name);
        mDatabase.push().setValue(userData);

    }



}



























































/*import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.joy.Constants;
import com.android.joy.Helper.Facebookuser;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;


import com.android.joy.R;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 9001;
    ImageView imageView;

    TextView textView;

    LoginButton loginButton;

    private AccessToken mAccessToken;

    private CallbackManager callbackManager;

    private FirebaseAuth mAuth;

    private GoogleSignInOptions gso;

    private GoogleSignInClient mGoogleSignInClient;

    private SignInButton signInButton;

    @Override
    protected void onStart()
    {
        super.onStart();


        FirebaseAuth mAuth1 = FirebaseAuth.getInstance();

        GoogleSignInOptions gso1 = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        GoogleSignInClient mGoogleSignInClient1 = GoogleSignIn.getClient(this, gso1);

        LoginManager.getInstance().logOut();
        mAuth1.signOut();
        mGoogleSignInClient1.signOut().
                addOnCompleteListener(
                        this,
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Log.d("Success", "Google Sign out successfull");
                            }
                        });

*/

        /*
        Intent i = getIntent();


        String warningMessage = i.getStringExtra("warning");

        if (warningMessage != null) {

            Log.d("WarMes", warningMessage);
        }

        if (warningMessage == null) {

            FirebaseUser currentUser = mAuth.getCurrentUser();

            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

            boolean allLogedOut = currentUser == null && account == null;

            if (account != null) {
                Log.d("NNN", account.getDisplayName());
            }

            Intent intent = getIntent();

            String message = intent.getStringExtra("message");

            if (message == null) {
                updateUI(currentUser);
            } else {
                mAuth.signOut();
            }
        }
        */

/*
}

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        registerViews();

        registerListeners();

    }
    private  void registerViews()
    {
        imageView = findViewById(R.id.imageViewLogin);

        textView = findViewById(R.id.textViewLogin);

        loginButton = findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList(Constants.EMAIL));

        callbackManager = CallbackManager.Factory.create();

        mAuth = FirebaseAuth.getInstance();

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("341224336496-6149tbf5fv5psk0kap9skq0801gl28n2.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        signInButton = findViewById(R.id.sign_in_button);
    }

    private void registerListeners()
    {

        loginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
                {
                    @Override
                    public void onSuccess(LoginResult loginResult)
                    {
                        mAccessToken = loginResult.getAccessToken();

                        //getUserProfile(mAccessToken);

                        //String email = handleFacebookAccessToken(mAccessToken);

                        FirebaseUser currentUser = mAuth.getCurrentUser();

                        if (currentUser != null)
                        {
                            String name = currentUser.getDisplayName();
                            String email = currentUser.getEmail();
                            Uri photoUrl = currentUser.getPhotoUrl();

                            Intent intent = new Intent(LoginActivity.this, CategoriesScreenActivity.class);
                            //String name = getUserProfile(mAccessToken);

                            intent.putExtra("name", name);
                            intent.putExtra("email", email);
                            intent.putExtra("url", photoUrl.toString());

                            finish();

                            startActivity(intent);

                        }

                        //handleFacebookAccessToken(mAccessToken);

                        //LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onCancel()
                    {

                    }

                    @Override
                    public void onError(FacebookException error)
                    {

                    }
                });
            }
        });


        // registering google sign in button click listener

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                signInIntent.putExtra("warning", "this intent is for activity result so please skip the on start method results");

                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

    }

    private void updateDataToFirebase(AccessToken mAccessToken)
    {

    }

    private String handleFacebookAccessToken(AccessToken mAccessToken)
    {
        final String[] temp = {null};
        AuthCredential credential = FacebookAuthProvider.getCredential(mAccessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (task.isSuccessful())
                        {
                            FirebaseUser user = mAuth.getCurrentUser();
                            temp[0] = user.getEmail();
                            Log.d("RC_SIGN_IN RESULT_OK", user.getUid() + " " + user.getEmail());
                        }
                        else
                        {
                            Log.d("Err", "Authentication failed");
                        }
                    }
                });
        return temp[0];
    }

    private String getUserProfile(AccessToken mAccessToken)
    {
        final Bundle bundle = new Bundle();

        final Facebookuser facebookuser = new Facebookuser();

        final String[] temp = {null};

        // Getting name of the user
        final GraphRequest request = GraphRequest.newMeRequest(mAccessToken,
                new GraphRequest.GraphJSONObjectCallback()
                {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response)
                    {
                        String name = null;
                        String profilePic = null;
                        try
                        {
                            name = object.get("name").toString();
                            temp[0] = name;
                            Log.d("FbUser", name);
                            facebookuser.setName(name);
                            bundle.putString("name", name);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                        Log.d("Name", name);
                    }
                });
        request.executeAsync();

        facebookuser.setEmail("");

        return temp[0];
    }

    private void updateUI(FirebaseUser currentUser)
    {
        // checking if user is already logged in or not

        if (currentUser != null)
        {
            String name = currentUser.getDisplayName();
            String email = currentUser.getEmail();
            Uri photoUrl = currentUser.getPhotoUrl();

            Intent intent = new Intent(LoginActivity.this, CategoriesScreenActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("email", email);
            intent.putExtra("url", photoUrl.toString());

            startActivity(intent);

            //LoginManager.getInstance().logOut();
            Log.d("Yes",  name + ", " + email + ", " + photoUrl);
        } else {
            Log.d("No", "Logout");
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("GGG", "RCode is " + resultCode + " and RC_S is " + RC_SIGN_IN);

        if (resultCode == RC_SIGN_IN)
        {

            Log.d("GGG", "Yes");

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        callbackManager.onActivityResult(requestCode, resultCode,  data);

    }

    private void firebaseAuthenticationWithGoogle(GoogleSignInAccount account)
    {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d("SSS", "Success Google");
                        } else {
                            Log.d("EEE", "Error Authenticating");
                        }
                    }
                });
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try
        {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.d("GGG", account.getId());
            firebaseAuthenticationWithGoogle(account);
        }
        catch (ApiException e)
        {
            Log.d("GGG", "signInResult:failed code=" + e.getStatusCode());
            e.printStackTrace();
        }
    }

}

*/


