package com.android.joy.Activities;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.joy.Adapters.SwipeAdapter;
import com.android.joy.Constants;
import com.android.joy.Helper.AdmobAdHelper;
import com.android.joy.Helper.DataHelper;
import com.android.joy.Helper.FirebaseData;
import com.android.joy.Helper.HandlingQuoteData;
import com.android.joy.R;
import com.android.joy.Room.UserDatabase;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class MorningQuotes extends AppCompatActivity implements View.OnClickListener
{
    boolean isOnLastPage = false;

    int timerSeconds;

    boolean userWatchedTheAd = false;

    public static UserDatabase userDatabase;

    ViewPager viewPager;

    SwipeAdapter swipeAdapter;

    TextView textMorningFragmentDay,
            textLikeMorningFragment,
            textSavedMorningFragment,
            textShareMorningFragment,
            uselessTextViewMorningFragment,
            textNotificationMorningFragment;

    ImageView iconNotificationMorningFragment, favMorning;

    Spinner dayOfMorningFragment,
            timeOfMorningFragment;

    int[] slider_images = {R.drawable.quote_box, R.drawable.quote_box, R.drawable.quote_box};

    String[] quotes;
    String[] owners;

    ProgressBar morningProgress;

    TextView noInternetTextView;

    TextView morningDate;
    private AdmobAdHelper admobAdHelper;

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_morning_quotes);

        registerViews();

        hideUnusedThings();

        createAndSetToolBar();

        trackViewPagerListener();

        showQuotes();

        registerListenersNew();

        trackViewPagerListener();

    }

    private void showQuotes()
    {
        Toast.makeText(MorningQuotes.this, "Please wait while quotes been loaded!", Toast.LENGTH_SHORT).show();
        FirebaseData.getInstance().initializeAdapter(FirebaseDatabase.getInstance().getReference().child("Quotes").child("Categories").child("Morning").getRef(), viewPager, swipeAdapter, this, slider_images, quotes, owners, morningProgress, noInternetTextView, textShareMorningFragment, favMorning);
    }

    private Thread createTimerThread()
    {
        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                timerSeconds = Calendar.getInstance().get(Calendar.SECOND);
                final int minute = Calendar.getInstance().get(Calendar.MINUTE);

                while(isOnLastPage)
                {
                    if (!userWatchedTheAd) {
                        if (isOnLastPage) {
                            try {
                                Thread.sleep(1000);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        int currSecond = Calendar.getInstance().get(Calendar.SECOND);
                                        int currMinute = Calendar.getInstance().get(Calendar.MINUTE);
                                        if (currMinute - minute == 1) {
                                            currSecond -= 60;
                                            if (currSecond - timerSeconds >= 5) {
                                                    admobAdHelper.displayAd();
                                                    userWatchedTheAd = true;
                                            }
                                        } else if (currMinute - minute == 0) {
                                            if (currSecond - timerSeconds >= 5) {
                                                    admobAdHelper.displayAd();
                                                    userWatchedTheAd = true;
                                            }
                                        }
                                    }
                                });
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        };
        return t;
    }

    private void registerListenersNew()
    {
        favMorning.setOnClickListener(this);
        textShareMorningFragment.setOnClickListener(this);
        /*mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed()
            {
                userWatchedTheAd = true;
            }
        });*/
    }

    private void trackViewPagerListener()
    {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int i, float v, int i1) { }

            @Override
            public void onPageSelected(int i)
            {
                if (i == 2) {
                    // start a timer here to check, if 5 second gets passes then it means we have to show the add
                    isOnLastPage = true;
                    Thread t = createTimerThread();
                    t.start();
                } else { isOnLastPage = false; }

            }

            @Override
            public void onPageScrollStateChanged(int i)
            {
                int item = viewPager.getCurrentItem();
                String text = quotes[item];
                if (HandlingQuoteData.getInstance().checkIfDataAlreadyExists(text)) {
                    favMorning.setTag("heart");
                    favMorning.setImageResource(R.drawable.heart);
                } else {
                    favMorning.setTag("like");
                    favMorning.setImageResource(R.drawable.like_icon);
                }
            }
        });
    }

    private void createAndSetToolBar()
    {
        Toolbar toolbar = findViewById(R.id.toolbarMorning);
        setSupportActionBar(toolbar);
        toolbar.setTitle(Constants.MORNING_QUOTE);
        toolbar.setNavigationIcon(Constants.BACK_ARROW);
        pressBackButton(toolbar); // Handling on back button pressed
        toolbar.setTitleTextColor(getResources().getColor(Constants.DOTS_COLOR_WHITE));
    }

    private void registerViews()
    {
        quotes = new String[3];

        owners = new String[3];

        textMorningFragmentDay = findViewById(R.id.textMorningFragmentDay);

        textLikeMorningFragment = findViewById(R.id.textLikesMorningFragment);

        textSavedMorningFragment = findViewById(R.id.textSavedMorningFragment);

        textShareMorningFragment = findViewById(R.id.textShareMorningFragment);

        uselessTextViewMorningFragment = findViewById(R.id.uselessTextViewMorningFragment);

        textNotificationMorningFragment = findViewById(R.id.textNotificationMorningFragment);

        iconNotificationMorningFragment = findViewById(R.id.iconNotificationMorningFragment);

        favMorning = findViewById(R.id.favImageMorning);

        dayOfMorningFragment = findViewById(R.id.spinnerDayMorningFragment);

        timeOfMorningFragment = findViewById(R.id.spinnerTimeMorningFragment);

        viewPager = findViewById(R.id.viewPagerFragmentMorning);

        morningProgress = findViewById(R.id.morning_progress);

        noInternetTextView = findViewById(R.id.textViewNoInternetMorning);

        favMorning.setTag("like");

        favMorning.setEnabled(false);

        userDatabase = Room.databaseBuilder(MorningQuotes.this, UserDatabase.class, Constants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        textShareMorningFragment.setEnabled(false);

        morningDate = findViewById(R.id.date_morning);

        DataHelper.getInstance().setDateText(morningDate, textMorningFragmentDay);

        admobAdHelper = new AdmobAdHelper(MorningQuotes.this, getResources().getString(R.string.test_google_app_id), getResources().getString(R.string.test_google_ad_unit_id), getResources().getString(R.string.client_test_device_id));

        DataHelper.getInstance().initializeSpinners(MorningQuotes.this, dayOfMorningFragment, timeOfMorningFragment);
    }

    private void pressBackButton(Toolbar toolbar)
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void hideUnusedThings()
    {
        textLikeMorningFragment.setVisibility(View.INVISIBLE);

        textSavedMorningFragment.setVisibility(View.INVISIBLE);

        uselessTextViewMorningFragment.setVisibility(View.INVISIBLE);

        textNotificationMorningFragment.setVisibility(View.INVISIBLE);

        iconNotificationMorningFragment.setVisibility(View.INVISIBLE);

        dayOfMorningFragment.setVisibility(View.INVISIBLE);

        dayOfMorningFragment.setEnabled(false);

        timeOfMorningFragment.setVisibility(View.INVISIBLE);

        timeOfMorningFragment.setEnabled(false);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.textShareMorningFragment:
                int i = viewPager.getCurrentItem();
                String text = quotes[i];
                HandlingQuoteData.getInstance().shareQuote(MorningQuotes.this, text);
                break;
            case R.id.favImageMorning:
                String quote = "";
                int item = viewPager.getCurrentItem();
                quote = quotes[item];
                String owner = owners[item];
                HandlingQuoteData.getInstance().addQuoteToFavourites(MorningQuotes.this, quote, owner, favMorning, morningProgress);
                break;
        }
    }
}
