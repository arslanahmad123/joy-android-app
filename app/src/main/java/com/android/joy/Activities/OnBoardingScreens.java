package com.android.joy.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.joy.Adapters.SliderAdapter;
import com.android.joy.Constants;
import com.android.joy.R;

public class OnBoardingScreens extends AppCompatActivity
{

    ViewPager mSliderViewPager;
    LinearLayout mDotsLayout;
    SliderAdapter mSliderAdapter;
    TextView[] mDots;
    Button skipBtn;
    int mCurrentPage;

    @Override
    protected void onStart()
    {
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_OPEN_FIRST_TIME, MODE_PRIVATE);
        if (sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_OPEN))
        {
            if (sharedPreferences.getBoolean(Constants.SHARED_PREFERENCES_KEY_OPEN, false))
            {
                moveToNextActivity();
            }
        }
    }

    private void moveToNextActivity()
    {
        Intent intent = new Intent(OnBoardingScreens.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_on_boarding_screens);

        registerViews();

        addDotsIndicator(0);

        registerListeners();
    }

    private void addDotsIndicator(int position)
    {
        mDots = new TextView[3];
        mDotsLayout.removeAllViews();

        for (int i=0; i<mDots.length; i++)
        {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(30);
            mDots[i].setTextColor(getResources().getColor(Constants.DOTS_COLOR_TRANSPARENT));
            mDotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0)
        {
            // checking if the number of pages to display in on-boarding screens are not zero.
            mDots[position].setTextColor(getResources().getColor(Constants.DOTS_COLOR_WHITE));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageScrolled(int i, float v, int i1) { if (v != 0) { } else { } }

        @Override
        public void onPageSelected(int i)
        {
            addDotsIndicator(i);
            mCurrentPage = i;
        }

        @Override
        public void onPageScrollStateChanged(int i) { }
    };

    private void registerViews()
    {
        mSliderViewPager = findViewById(R.id.viewPager);
        mDotsLayout = findViewById(R.id.dotsLayout);
        skipBtn = findViewById(R.id.buttonSkip);

        mSliderAdapter = new SliderAdapter(this);
        mSliderViewPager.setAdapter(mSliderAdapter);

        mSliderViewPager.addOnPageChangeListener(viewListener);
    }

    private void registerListeners()
    {
        skipBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(OnBoardingScreens.this, LoginActivity.class);
                setUserPrefrenceAboutFirstTimeOpen();
                startActivity(intent);
                finish();
            }
        });
    }

    private void setUserPrefrenceAboutFirstTimeOpen()
    {
        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES_OPEN_FIRST_TIME, MODE_PRIVATE);
        if (!preferences.contains(Constants.SHARED_PREFERENCES_KEY_OPEN))
        {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(Constants.SHARED_PREFERENCES_KEY_OPEN, true);
            editor.commit();
        }
    }

}
