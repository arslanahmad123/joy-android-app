package com.android.joy.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.joy.Adapters.TimeAdapter;
import com.android.joy.Constants;
import com.android.joy.R;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity
{

    TextView notificationTitle, textMorning, textAfternoon, textEvening, textNotificationOnOff;

    CheckBox checkBoxOnOff;

    ImageView notificationImage;

    Spinner morning, afternoon, evening;

    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        registerViews();
        createAndSetToolBar();
        registerListerners();
        initializeSpinners();
    }

    private void registerViews()
    {
        // registering text views
        notificationTitle = findViewById(R.id.textNotificationTitle);
        textMorning = findViewById(R.id.textViewMorningTime);
        textAfternoon = findViewById(R.id.textViewAfternoonTime);
        textEvening = findViewById(R.id.textViewEveningTime);

        // checkbox
        checkBoxOnOff = findViewById(R.id.checkBoxOnOff);

        setBackendValueToCheckBox();

        notificationImage = findViewById(R.id.imageNotification);

        // checked text view
        textNotificationOnOff = findViewById(R.id.checkedTextViewOnOff);

        // spinners
        morning = findViewById(R.id.spinnerMorningTime);
        afternoon = findViewById(R.id.spinnerAfternoonTime);
        evening = findViewById(R.id.spinnerEveningTime);

        // button
        saveBtn = findViewById(R.id.save_settings_btn);
    }

    private void setBackendValueToCheckBox()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        boolean hasKey = sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_NOTIFICATION);
        if (hasKey)
        {
            boolean value = sharedPreferences.getBoolean(Constants.SHARED_PREFERENCES_KEY_NOTIFICATION, false);
            if (value)
            {
                checkBoxOnOff.setChecked(true);
            }
            else
            {
                checkBoxOnOff.setChecked(false);
            }
        }
        else
        {
            checkBoxOnOff.setChecked(true);
        }
    }

    private void createAndSetToolBar()
    {
        Toolbar toolbar = findViewById(R.id.toolbarSetting);
        setSupportActionBar(toolbar);
        //toolbar.setTitle(Constants.MORNING_QUOTE);
        toolbar.setNavigationIcon(Constants.BACK_ARROW);
        pressBackButton(toolbar); // Handling on back button pressed
        toolbar.setTitleTextColor(getResources().getColor(Constants.DOTS_COLOR_WHITE));
    }

    private void pressBackButton(Toolbar toolbar)
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void registerListerners()
    {
        saveBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(SettingsActivity.this, CategoriesScreenActivity.class);

                if (checkBoxOnOff.isChecked()) {
                    saveUserPrefNotification(true);
                }
                else {
                    saveUserPrefNotification(false);
                }
                setUserPrefNotificationTime();
                startActivity(intent);
                finish();
            }
        });
    }

    private void saveUserPrefNotification(boolean value)
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.SHARED_PREFERENCES_KEY_NOTIFICATION, value);
        editor.commit();
    }

    private void setUserPrefNotificationTime()
    {
        String morningTimeValue = morning.getSelectedItem().toString();
        String afternoonTimeValue = afternoon.getSelectedItem().toString();
        String eveningTimeValue = evening.getSelectedItem().toString();

        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SHARED_PREFERENCES_KEY_MORNING, morningTimeValue);
        editor.putString(Constants.SHARED_PREFERENCES_KEY_AFTERNOON, afternoonTimeValue);
        editor.putString(Constants.SHARED_PREFERENCES_KEY_EVENING, eveningTimeValue);
        editor.commit();
    }

    private void initializeSpinners()
    {
        String[] morningTime;
        morningTime = getResources().getStringArray(R.array.morningTimes);

        String[] afternoonTime;
        afternoonTime = getResources().getStringArray(R.array.afternoonTimes);

        String[] eveningTime;
        eveningTime = getResources().getStringArray(R.array.eveningTimes);

        ArrayList<String> listMorning = new ArrayList<>();

        ArrayList<String> listAfternoon = new ArrayList<>();

        ArrayList<String> listEvening = new ArrayList<>();

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);

        String morValue = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_MORNING, "");
        String aftValue = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_AFTERNOON, "");
        String eveValue = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_EVENING, "");

        TimeAdapter morningAdapter = null, afternoonAdapter = null, eveningAdapter = null;

        // for morning

        if (morValue.isEmpty())
        {
            listMorning = addData(morningTime);
            settingAdapter(morning, morningAdapter, listMorning);
        }
        else
        {
            listMorning.add(morValue);
            ArrayList<String> list = addData(morningTime, morValue);
            listMorning.addAll(list);
            settingAdapter(morning, morningAdapter, listMorning);
        }

        // for afternoon

        if (aftValue.isEmpty())
        {
            listAfternoon = addData(afternoonTime);
            settingAdapter(afternoon, afternoonAdapter, listAfternoon);
        }
        else
        {
            listAfternoon.add(aftValue);
            ArrayList<String> list = addData(afternoonTime, aftValue);
            listAfternoon.addAll(list);
            settingAdapter(afternoon, afternoonAdapter, listAfternoon);
        }

        // for evening

        if (eveValue.isEmpty())
        {
            listEvening = addData(eveningTime);
            settingAdapter(evening, eveningAdapter, listEvening);
        }
        else
        {
            listEvening.add(eveValue);
            ArrayList<String> list = addData(eveningTime, eveValue);
            listEvening.addAll(list);
            settingAdapter(evening, eveningAdapter, listEvening);
        }

    }

    private void settingAdapter(Spinner spinner, TimeAdapter adapter, ArrayList list)
    {
        adapter = new TimeAdapter(list, this);
        spinner.setAdapter(adapter);
    }

    private ArrayList<String> addData(String[] times)
    {
        ArrayList<String> list = new ArrayList<>();
        for (int i=0; i<times.length; i++)
        {
            list.add(times[i]);
        }
        return list;
    }

    private ArrayList<String> addData(String[] times, String value)
    {
        ArrayList<String> list = new ArrayList<>();
        for (int i=0; i<times.length; i++)
        {
            if (!times[i].equals(value))
            {
                list.add(times[i]);
            }
        }
        return list;
    }

}
