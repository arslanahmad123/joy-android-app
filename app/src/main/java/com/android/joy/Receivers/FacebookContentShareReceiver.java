package com.android.joy.Receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.android.joy.Activities.AfternoonQuotes;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.util.Objects;

public class FacebookContentShareReceiver extends BroadcastReceiver
{

    ShareDialog shareDialog;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d("Yes", "");


        //String selectedAppPackage = String.valueOf(intent.getExtras().get(Intent.EXTRA_TEXT));



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            for (String key : Objects.requireNonNull(intent.getExtras()).keySet()) {
                try {
                    ComponentName componentInfo = (ComponentName) intent.getExtras().get(key);
                    PackageManager packageManager = context.getPackageManager();
                    assert componentInfo != null;
                    String appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(componentInfo.getPackageName(), PackageManager.GET_META_DATA));
                    if (appName.equals("Facebook"))
                    {

                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setQuote(Intent.EXTRA_TEXT)
                                .setContentUrl(Uri.parse("https://www.google.com"))
                                .build();

                        // for facebook
                        shareDialog.show(linkContent);

                        ShareDialog shareDialog = new ShareDialog((Activity) context);

                        CallbackManager callbackManager = CallbackManager.Factory.create();

                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {

                            }

                            @Override
                            public void onCancel() {

                            }

                            @Override
                            public void onError(FacebookException error) {

                            }
                        });


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
