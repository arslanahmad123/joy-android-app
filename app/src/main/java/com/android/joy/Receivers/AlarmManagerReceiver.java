package com.android.joy.Receivers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.android.joy.R;

public class AlarmManagerReceiver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {

        Log.d("alarm_", "receiver");

        String body = intent.getStringExtra("body");
        String type = intent.getStringExtra("type");

        NotificationCompat.Builder builder;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        int id = 0;

        if (type.equals("morning"))
        {
            id = 1;
        }
        else if (type.equals("afternoon"))
        {
            id = 2;
        }
        else if (type.equals("evening"))
        {
            id = 3;
        }

        if (Build.VERSION.SDK_INT >= 26) {

            NotificationChannel mChannel = new NotificationChannel(Integer.toString(id), "channelId" + id, NotificationManager.IMPORTANCE_HIGH);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(mChannel);

            builder = new NotificationCompat.Builder(context, Integer.toString(id))
                    .setSmallIcon(R.mipmap.ic_launcher1)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSound(uri)
                    .setChannelId(Integer.toString(id))
                    .setAutoCancel(true);
        } else {
            builder = new NotificationCompat.Builder(context, Integer.toString(id))
                    .setSmallIcon(R.mipmap.ic_launcher1)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSound(uri)
                    .setAutoCancel(true);
        }
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(id, builder.build());
    }
}
