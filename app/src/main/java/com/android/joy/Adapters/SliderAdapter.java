package com.android.joy.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.joy.R;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    int[] slider_images = {R.drawable.onboard_1, R.drawable.onboard_2, R.drawable.onboard_3};
    String[] bold_text = {"Have a nice day!", "Good Afternoon!", "Good Evening!"};
    String[] paragraph_text = {"Make your morning beautiful with morning quotes", "Make your afternoon beautiful with afternoon quotes", "Make your evening beautiful with evening quotes"};

    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return slider_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);
        ImageView slideImageViewLogo = (ImageView) view.findViewById(R.id.imageViewScreensLogo);
        ImageView slideImageViewScreens = (ImageView) view.findViewById(R.id.imageViewScreens);
        slideImageViewScreens.setImageResource(slider_images[position]);
        TextView textBold = (TextView) view.findViewById(R.id.boldText);
        textBold.setText(bold_text[position]);
        TextView textPara = (TextView) view.findViewById(R.id.paragraphText);
        textPara.setText(paragraph_text[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
