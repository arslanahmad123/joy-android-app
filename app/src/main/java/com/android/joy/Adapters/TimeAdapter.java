package com.android.joy.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.joy.R;

import java.util.ArrayList;

public class TimeAdapter extends BaseAdapter
{

    private ArrayList<String> list;
    private Context context;
    private LayoutInflater layoutInflater;
    TextView listItem;

    public TimeAdapter(ArrayList<String> list, Context context)
    {
        this.list = list;
        this.context = context;
        this.layoutInflater = returnLAyoutInflater();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View custom_view = returnCustomView();
        listItem = custom_view.findViewById(R.id.textViewSingleListItem);
        listItem.setText(list.get(i));
        return custom_view;
    }

    private LayoutInflater returnLAyoutInflater()
    {
        return (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private View returnCustomView()
    {
        return layoutInflater.inflate(R.layout.time_layout_single_item, null);
    }
}
