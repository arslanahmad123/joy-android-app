package com.android.joy.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.joy.Constants;
import com.android.joy.R;

public class SwipeAdapter extends PagerAdapter
{

    int[] slider_images;

    String[] quotes;

    String[] owners;

    Context context;

    LayoutInflater layoutInflater;

    TextView textMorningQuotes;

    ImageView imageSliderFrameLayout;
    TextView ownerMorningFragment;

    public SwipeAdapter(Context context, int[] slider_images, String[] quotes, String[] owners)
    {
        this.context = context;
        this.slider_images = slider_images;
        this.quotes = quotes;
        this.owners = owners;
    }

    @Override
    public int getCount()
    {
        return slider_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o)
    {
        return view == (LinearLayout)o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position)
    {
        setLayoutInflater();

        return returnCustomView(container, position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object)
    {
        container.removeView((View) object);
    }

    private void setLayoutInflater()
    {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private View getCustomView(ViewGroup container)
    {
        View customView = layoutInflater.inflate(R.layout.image_slider_layout, container, false);
        return customView;
    }

    private void registerViews(View custom_view)
    {
        textMorningQuotes = custom_view.findViewById(R.id.textQuotesMorning);
        ownerMorningFragment = custom_view.findViewById(R.id.ownerQuotesMorning);
        imageSliderFrameLayout = custom_view.findViewById(R.id.imageViewSliderFrameLayout);
    }

    private void setViewsData(int position)
    {
        textMorningQuotes.setText(quotes[position]);
        textMorningQuotes.setMovementMethod(new ScrollingMovementMethod());
        ownerMorningFragment.setText(owners[position]);
        imageSliderFrameLayout.setImageResource(slider_images[position]);
    }

    private View returnCustomView(ViewGroup container, int position)
    {
        View custom_view = getCustomView(container);

        registerViews(custom_view);

        setViewsData(position);

        container.addView(custom_view);

        return custom_view;
    }

}
