package com.android.joy;

import com.android.joy.R;

public class Constants
{
    public static final int DOTS_COLOR_TRANSPARENT = R.color.colorTransparenWhite;

    public static final int DOTS_COLOR_WHITE = R.color.white;

    public static final int SPLASH_TIME_OUT = 1500;

    public static final String MORNING_QUOTE = "Morning Quotes";

    public static final int BACK_ARROW = R.drawable.ic_arrow_back_24dp;

    public static final String LOGOUT_MESSAGE = "Are you sure you want to logout?";

    public static final String EMAIL = "email";

    public static final int RC_SIGN_IN = 9001;

    public static final String AUTH_CLIENT_KEY = "341224336496-iclu82vl727l0kltnurcv52pseclaik8.apps.googleusercontent.com";

    public final static String SHARED_PREFERENCES_FILE_USER_INFO = "userInfo";

    public final static String SHARED_PREFERENCES_KEY_USER_INFO = "User_Info";

    public final static String SHARED_PREFERENCES_KEY_NOTIFICATION = "notification";

    public final static String SHARED_PREFERENCES_KEY_MORNING = "morning";

    public final static String SHARED_PREFERENCES_KEY_AFTERNOON = "afternoon";

    public final static String SHARED_PREFERENCES_KEY_EVENING = "evening";

    public final static String SHARED_PREFERENCES_OPEN_FIRST_TIME = "open";

    public final static String SHARED_PREFERENCES_KEY_OPEN = "opened";

    public final static String USER_INFO_TAG = "USER_INFO_TAG";

    public final static String OFFLINE_MESSAGE = "You are offline.";

    public static final String DATABASE_NAME = "fav_quotes";
}
