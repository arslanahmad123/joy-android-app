package com.android.joy.Services;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.android.joy.Activities.CategoriesScreenActivity;
import com.android.joy.Constants;
import com.android.joy.R;
import com.android.joy.Receivers.AlarmManagerReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MyFirebaseMessagingService extends FirebaseMessagingService
{

    private BroadcastReceiver receiver = new AlarmManagerReceiver();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {

        Log.d("Yes", "Entering");

        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        super.onMessageReceived(remoteMessage);
        boolean hasKey = sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_NOTIFICATION);

        // 1 for morning
        // 2 for afternoon
        // 3 for evening

        Calendar currCalendar = Calendar.getInstance();

        int currHour = currCalendar.get(Calendar.HOUR_OF_DAY);

        // only using one alarm manager.

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);


        if (hasKey) // means key is present and user has saved wether to see notifications or not
        {

            boolean value = sharedPreferences.getBoolean(Constants.SHARED_PREFERENCES_KEY_NOTIFICATION, false);

            if (value) // means key is present and user has enabled push notifications
            {

                if (currHour >= 6 && currHour < 12)
                {

                    boolean check = checkCustomNotificationMorning();

                    if (check) // check for custom time
                    {
                        // if time not equals to 6 am then use alarm manager
                        registerReceiverAtRuntime();
                        setALarmToAlarmManager(remoteMessage.getNotification().getBody(), "morning", 1);
                    }
                    else
                    {
                        // means you have not set custom time, so throw message independently
                        sendMyNotificationNew(remoteMessage.getNotification().getBody());
                    }
                }

                else if (currHour >= 12 && currHour < 18)
                {

                    boolean check = checkCustomNotificationAfternoon();


                    if (check)
                    {
                        // if time not equals to 12 pm then use alarm manager

                        Log.d("alarm_", "custom");
                        registerReceiverAtRuntime();
                        setALarmToAlarmManager(remoteMessage.getNotification().getBody(), "afternoon", 2);
                    }
                    else
                    {
                        // means you have not set custom time, so throw message independently
                        Log.d("alarm_", "default");
                        sendMyNotificationNew(remoteMessage.getNotification().getBody());
                    }
                }

                else if (currHour >= 18 && currHour < 24 )
                {

                    if (checkCustomNotificationEvening())
                    {
                        // if time not equals to 6pm then use alarm manager

                        registerReceiverAtRuntime();
                        setALarmToAlarmManager(remoteMessage.getNotification().getBody(), "evening", 3);
                    }
                    else
                    {
                        // means you have not set custom time, so throw message independently
                        sendMyNotificationNew(remoteMessage.getNotification().getBody());
                    }
                }
            }
        }
        else
        {
            // throw notification as soon as it is received
            sendMyNotificationNew(remoteMessage.getNotification().getBody());
        }

    }

    private void registerReceiverAtRuntime()
    {
        IntentFilter filter = new IntentFilter();
        getApplicationContext().registerReceiver(receiver, filter);
        registerReceiver(receiver, filter);
    }

    private void setALarmToAlarmManager(String body, String type, int requestCode)
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        Intent intent = new Intent(this, AlarmManagerReceiver.class);
        intent.putExtra("body", body);
        intent.putExtra("type", type);
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntentMorning = PendingIntent.getBroadcast(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        String key = "";
        if (type.equals("morning"))
        {
            key = Constants.SHARED_PREFERENCES_KEY_MORNING;
        }
        else if (type.equals("afternoon"))
        {
            key = Constants.SHARED_PREFERENCES_KEY_AFTERNOON;
        }
        else if (type.equals("evening"))
        {
            key = Constants.SHARED_PREFERENCES_KEY_EVENING;
        }

        String mTime = sharedPreferences.getString(key, "");
        String[] tokens = mTime.split(" ");
        String[] tokens1 = tokens[0].split(":");

        if (tokens.length > 0) {
            if (key.equals(Constants.SHARED_PREFERENCES_KEY_AFTERNOON) | key.equals(Constants.SHARED_PREFERENCES_KEY_EVENING)) {
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(tokens1[0]) + 12);
            } else {
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(tokens1[0]));
            }
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
        }

        Log.d("Current Time", calendar.getTime().toString());

        if (Build.VERSION.SDK_INT >= 19) {
            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntentMorning);
        } else {
            manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntentMorning);
        }
    }

    private boolean checkCustomNotificationMorning()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        boolean hasKey = sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_MORNING);
        if (hasKey)
        {
            String value = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_MORNING, "");
            String[] tokens = value.split(" ");
            StringBuilder builder = new StringBuilder();
            for (int i=0; i<tokens.length; i++)
            {
                builder.append(tokens[i]);
            }
            String mornTime = builder.toString();
            if (mornTime.equals("6:00AM"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false; // means if user has not set the custom time then the time is default
    }

    private boolean checkCustomNotificationAfternoon()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        boolean hasKey = sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_AFTERNOON);
        if (hasKey)
        {
            String value = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_AFTERNOON, "");
            String[] tokens = value.split(" ");
            StringBuilder builder = new StringBuilder();
            for (int i=0; i<tokens.length; i++)
            {
                builder.append(tokens[i]);
            }
            String afterTime = builder.toString();
            if (!afterTime.equals("12:00PM"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false; // means if user has not set the custom time then the time is default
    }

    private boolean checkCustomNotificationEvening()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_FILE_USER_INFO, MODE_PRIVATE);
        boolean hasKey = sharedPreferences.contains(Constants.SHARED_PREFERENCES_KEY_EVENING);
        if (hasKey)
        {
            String value = sharedPreferences.getString(Constants.SHARED_PREFERENCES_KEY_EVENING, "");
            String[] tokens = value.split(" ");
            StringBuilder builder = new StringBuilder();
            for (int i=0; i<tokens.length; i++)
            {
                builder.append(tokens[i]);
            }
            String eveTime = builder.toString();
            if (eveTime.equals("6:00PM"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false; // means if user has not set the custom time then the time is default
    }

    private void sendMyNotificationNew(String body)
    {
        int id = 0;

        Calendar calendar = Calendar.getInstance();

        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        if (hour < 12)
        {
            id = 0;
        }
        else if (hour >= 12 && hour < 18)
        {
            id = 1;
        }
        else if (hour >= 18 && hour <= 24)
        {
            id = 2;
        }

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(this, CategoriesScreenActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationChannel notificationChannel;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= 26)
        {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Integer.toString(id))
                    .setSmallIcon(R.mipmap.ic_launcher1)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSound(uri)
                    .setAutoCancel(true);

            notificationChannel = new NotificationChannel(Integer.toString(id), "channelId" + id, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(id, builder.build());
        }
        else
        {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), Integer.toString(id))
                    .setSmallIcon(R.mipmap.ic_launcher1)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSound(uri)
                    .setChannelId(Integer.toString(id))
                    .setAutoCancel(true);
            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
            notificationManagerCompat.notify(id, builder.build());
        }

    }









    /*


    private void sendMyNotificationUpdated(String body)
    {
        int id = 0;

        Calendar calendar = Calendar.getInstance();

        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        if (hour < 12)
        {
            id = 0;
        }
        else if (hour >= 12 && hour < 18)
        {
            id = 1;
        }
        else if (hour >= 18 && hour <= 24)
        {
            id = 2;
        }
        Intent intent = new Intent(this, CategoriesScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (Build.VERSION.SDK_INT >= 26)
        {
            NotificationChannel mChannel = new NotificationChannel(Integer.toString(id), "channelId" + id, NotificationManager.IMPORTANCE_HIGH);

            NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(mChannel);

            builder = new NotificationCompat.Builder(getApplicationContext(), Integer.toString(id))
                    .setSmallIcon(R.mipmap.ic_launcher1)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSound(uri)
                    .setChannelId(Integer.toString(id))
                    .setAutoCancel(true);

        }
        else
        {
            builder = new NotificationCompat.Builder(getApplicationContext(), Integer.toString(id))
                    .setSmallIcon(R.mipmap.ic_launcher1)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSound(uri)
                    .setAutoCancel(true);
        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        notificationManagerCompat.notify(id, builder.build());

    }

    private void sendMyNotification(String body)
    {
        Intent intent = new Intent(this, CategoriesScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher1)
                .setContentText(body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


    */

}
