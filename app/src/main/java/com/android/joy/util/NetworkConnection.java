package com.android.joy.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

public class NetworkConnection
{
    public static boolean isConnectingToInternet(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network[] networks;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
        {
            networks = connectivityManager.getAllNetworks();
            for (Network network : networks)
            {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                if (networkInfo
                        .getState()
                        .equals(NetworkInfo
                                .State
                                .CONNECTED))
                {
                    return true;
                }
            }
        }
        else
        {
            if (connectivityManager != null)
            {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null)
                {
                    for (NetworkInfo netInfo : info)
                    {
                        if (netInfo.getState()
                                ==
                                NetworkInfo
                                .State
                                .CONNECTED)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
