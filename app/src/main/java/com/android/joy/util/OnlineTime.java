package com.android.joy.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.android.joy.Activities.CategoriesScreenActivity;

import org.apache.commons.net.ftp.FTPHTTPClient;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.net.InetAddress;

public class OnlineTime
{
        //NTP server list: http://tf.nist.gov/tf-cgi/servers.cgi
        public static final String TIME_SERVER = "pool.ntp.org";

        private static OnlineTime instance = null;

        public static OnlineTime getInstance()
        {
            if (instance == null)
            {
                instance = new OnlineTime();
            }
            return instance;
        }

        public long getCurrentNetworkTime() {
            NTPUDPClient lNTPUDPClient = new NTPUDPClient();
            lNTPUDPClient.setDefaultTimeout(3000);
            long returnTime = 0;
            try {
                lNTPUDPClient.open();
                InetAddress lInetAddress = InetAddress.getByName(TIME_SERVER);
                TimeInfo lTimeInfo = lNTPUDPClient.getTime(lInetAddress);
                // returnTime =  lTimeInfo.getReturnTime(); // local time
                returnTime = lTimeInfo.getMessage().getTransmitTimeStamp().getTime();   //server time

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lNTPUDPClient.close();
            }

            return returnTime;
        }

        public String getCurrDateFromGPS()
        {

            return "";
        }
}
