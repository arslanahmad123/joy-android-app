package com.android.joy.Helper;

import com.google.firebase.auth.FirebaseAuth;

public class FirebaseLoginHelper
{
    private FirebaseAuth mAuth;

    public FirebaseLoginHelper(FirebaseAuth mAuth)
    {
        this.mAuth = mAuth;
    }

    public FirebaseAuth getmAuth()
    {
        return mAuth;
    }
}
