package com.android.joy.Helper;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.joy.Activities.LoginActivity;
import com.android.joy.Activities.MorningQuotes;
import com.android.joy.Room.User;
import com.android.joy.util.NetworkConnection;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class HandlingQuoteData
{
    private static HandlingQuoteData instance;

    public static HandlingQuoteData getInstance()
    {
        if (instance == null)
        {
            instance = new HandlingQuoteData();
        }
        return instance;
    }

    public void shareQuote(Context context, String text)
    {
        if (NetworkConnection.isConnectingToInternet(context))
        {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "");
            intent.putExtra(Intent.EXTRA_TEXT, text + "\nDownload Joy for more quotes\n" + "https://www.google.com");
            context.startActivity(Intent.createChooser(intent, "Choose Sharing Method"));
        }
        else
        {
            Toast.makeText(context, "No internet found!", Toast.LENGTH_SHORT).show();
        }
    }

    public void addToFav(Context context, String text, String owner, ImageView favMorning)
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        AddingFavouriteQuotesHelper object = new AddingFavouriteQuotesHelper(context, user.getUid().toString(), user.getDisplayName().toString(), text, favMorning, owner);
        object.addQuoteToFavourite();
    }

    public void addQuoteToFavourites(Context context, String quote, String owner, ImageView favMorning, ProgressBar morningProgress)
    {
        if (NetworkConnection.isConnectingToInternet(context))
        {

            if (checkIfDataAlreadyExists(quote)) {
                // already exists
                Toast.makeText(context, "Already Added to favourite quotes", Toast.LENGTH_SHORT).show();
            } else {
                morningProgress.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Please wait for the quote to be added", Toast.LENGTH_SHORT).show();
                HandlingQuoteData.getInstance().addToFav(context, quote, owner, favMorning);
                morningProgress.setVisibility(View.INVISIBLE);
                Toast.makeText(context, "Added to favourite quotes", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(context, "You are not connected to internet !", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkIfDataAlreadyExists(String text)
    {
        List<User> users = LoginActivity.userDatabase.daoAccess().getUsers();

        boolean hasValue = false;

        for (int i=0; i<users.size(); i++)
        {
            User user = users.get(i);
            if (user.getQuotes().equals(text))
            {
                hasValue = true;
                break;
            }
        }
        if (hasValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
