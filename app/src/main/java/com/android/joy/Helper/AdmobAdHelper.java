package com.android.joy.Helper;

import android.content.Context;

import com.android.joy.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.Timer;

public class AdmobAdHelper
{
    Context context;

    String appId, addUnitId, testDeviceId;

    public InterstitialAd mInterstitialAd;

    AdRequest request;

    public AdmobAdHelper(Context context, String appId, String addUnitId, String testDeviceId)
    {
        this.context = context;
        this.appId = appId;
        this.addUnitId = addUnitId;
        this.testDeviceId = testDeviceId;
        initializeInterstitialAd();
    }

    public void initializeInterstitialAd()
    {
        MobileAds.initialize(context, appId);
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(addUnitId);
        request = new AdRequest.Builder().addTestDevice(testDeviceId).build();
        mInterstitialAd.loadAd(request);
    }

    public void displayAd()
    {
        if (mInterstitialAd.isLoaded())
        {
            mInterstitialAd.show();
        }
    }

}
