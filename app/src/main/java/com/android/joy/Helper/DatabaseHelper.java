package com.android.joy.Helper;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.android.joy.Constants;
import com.android.joy.Room.UserDatabase;
import com.android.joy.Room.User;

public class DatabaseHelper
{
    Context context;
    private UserDatabase userDatabase;
    public DatabaseHelper(Context context)
    {
        this.context = context;
        userDatabase = Room.databaseBuilder(context, UserDatabase.class, Constants.DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
    }

    public UserDatabase getUserDatabase()
    {
        return userDatabase;
    }
}
