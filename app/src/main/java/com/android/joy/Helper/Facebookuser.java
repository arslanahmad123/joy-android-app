package com.android.joy.Helper;

import android.net.Uri;

public class Facebookuser
{
    private String name;
    private String imageUri;
    private String email;
    private String accountType;

    public Facebookuser()
    {
        name = "";
        imageUri = null;
        email = "";
        accountType = "";
    }

    public Facebookuser(String name, String photoUrl, String email, String accountType)
    {
        this.name = name;
        this.imageUri = photoUrl;
        this.email = email;
        this.accountType = accountType;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getAccountType()
    {
        return accountType;
    }

    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    @Override
    public String toString() {
        return "Facebookuser{" +
                "name='" + name + '\'' +
                ", photoUrl=" + imageUri +
                ", email='" + email + '\'' +
                ", accountType='" + accountType + '\'' +
                '}';
    }
}
