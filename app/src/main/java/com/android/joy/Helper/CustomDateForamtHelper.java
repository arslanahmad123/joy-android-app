package com.android.joy.Helper;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public final class CustomDateForamtHelper
{

    private static CustomDateForamtHelper instance = null;

    public static CustomDateForamtHelper getInstance()
    {
        if (instance == null)
        {
            instance = new CustomDateForamtHelper();
        }
        return instance;
    }

    public String theMonth(int month)
    {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    public String getTodaysDate()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = mDateFormat.format(calendar.getTime());
        String[] tokens = strDate.split("-");
        ArrayList<String> dateResult = new ArrayList<>();

        int currDate = Integer.parseInt(tokens[0]);

        if (tokens.length == 3)
        {
            dateResult.add(Integer.toString(currDate));
            dateResult.add(theMonth(Integer.parseInt(tokens[1]) - 1));
            dateResult.add(tokens[2]);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i < tokens.length; i++)
        {
            stringBuilder.append(dateResult.get(i));
        }

        String finalResult = stringBuilder.toString();

        return finalResult;
    }

    public String getPreviousDate()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = mDateFormat.format(calendar.getTime());
        String[] tokens = strDate.split("-");
        ArrayList<String> dateResult = new ArrayList<>();

        int currDate = Integer.parseInt(tokens[0]);

        if (tokens.length == 3)
        {
            dateResult.add(Integer.toString(currDate - 1));
            dateResult.add(theMonth(Integer.parseInt(tokens[1]) - 1));
            dateResult.add(tokens[2]);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i < tokens.length; i++)
        {
            stringBuilder.append(dateResult.get(i));
        }

        String finalResult = stringBuilder.toString();

        return finalResult;
    }

    public String formatToCustomDate(String date)
    {
        String[] tokens = date.split("-");
        ArrayList<String> dateResult = new ArrayList<>();

        int currDate = Integer.parseInt(tokens[0]);

        if (tokens.length == 3)
        {
            dateResult.add(Integer.toString(currDate));
            dateResult.add(theMonth(Integer.parseInt(tokens[1]) - 1));
            dateResult.add(tokens[2]);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i < tokens.length; i++)
        {
            stringBuilder.append(dateResult.get(i));
        }

        String finalResult = stringBuilder.toString();

        return finalResult;
    }

    public String returnDate(String status)
    {
        switch(status)
        {
            case "anotherday":
                String prevDate = getPrevDate("anotherday");
                String customDate = formatToCustomDate(prevDate);
                return customDate;
        }

        return "";
    }

    private String getPrevDate(String anotherday)
    {
        Calendar curr = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String currDate = format.format(curr.getTime());
        try {
            Date cDate = format.parse(currDate);
            Calendar prev = Calendar.getInstance();
            prev.setTime(cDate);
            prev.add(Calendar.DATE, -1);
            String prevDate = format.format(prev.getTime());
            Log.d("Current is ", currDate);
            Log.d("Previous is ", prevDate);
            return prevDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public int[] checkForCurrentTime()
    {
        Calendar calendar = Calendar.getInstance();
        int currHour = calendar.get(Calendar.HOUR_OF_DAY);
        int currMinute = calendar.get(Calendar.MINUTE);
        int currSec = calendar.get(Calendar.SECOND);

        if (currHour > 12 && currMinute >= 0)
        {
            System.out.println("Time is " + (currHour - 12) + ":" + currMinute + " PM");
        }
        else {
            System.out.println("Time is " + currHour + ":" + currMinute + " AM");
        }
        return new int[]{currHour, currMinute, currSec};
    }

    public String returnPeriodOfTime()
    {
        int[] currentTime = checkForCurrentTime();
        if (currentTime[0] >= 6 && currentTime[0] < 12)
        {
            return "morning";
        }
        else if (currentTime[0] >= 12 && currentTime[0] < 18)
        {
            return "afternoon";
        }
        else if (currentTime[0] >= 18 && currentTime[0] < 25)
        {
            return "evening";
        }
        else
        {
            return "anotherday";
        }
    }

    public boolean checkForEquals(String a, String b)
    {
        if (a.equals(b))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
