package com.android.joy.Helper;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public class GoogleLoginHelper
{
    private GoogleSignInOptions googleSignInOptions;

    private GoogleSignInClient mGoogleSignInClient;

    public GoogleLoginHelper(GoogleSignInOptions googleSignInOptions, GoogleSignInClient mGoogleSignInClient)
    {
        this.googleSignInOptions = googleSignInOptions;
        this.mGoogleSignInClient = mGoogleSignInClient;
    }

    public GoogleSignInOptions getGoogleSignInOptions()
    {
        return googleSignInOptions;
    }

    public GoogleSignInClient getmGoogleSignInClient()
    {
        return mGoogleSignInClient;
    }
}
