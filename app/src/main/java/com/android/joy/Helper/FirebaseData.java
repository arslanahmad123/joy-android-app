package com.android.joy.Helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.joy.Activities.MorningQuotes;
import com.android.joy.Adapters.SwipeAdapter;
import com.android.joy.R;
import com.android.joy.Room.User;
import com.android.joy.util.NetworkConnection;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.List;

public class FirebaseData
{
    private static FirebaseData instance;

    public static FirebaseData getInstance()
    {
        if (instance == null)
        {
            instance = new FirebaseData();
        }
        return instance;
    }

    public void initializeAdapter(DatabaseReference mDatabase, ViewPager viewPager, SwipeAdapter swipeAdapter, Context context, int[] slider_images, String[] quotes, String[] owners, ProgressBar progress, TextView alertText, TextView share, ImageView fav)
    {
        //initAndSetEmptyAdapter(viewPager, swipeAdapter, context, slider_images, quotes, owners);

        if (NetworkConnection.isConnectingToInternet(context))
        {
            showQuotesToUser(mDatabase, progress, alertText, quotes, owners, viewPager, swipeAdapter, context, slider_images, share, fav);
        }
        else
        {
            populateEmptyDataAndDisplayMessage(viewPager, swipeAdapter, context, slider_images, quotes, owners, progress, alertText, "Sorry, no internet found!", fav);
        }

    }

    public void showQuotesToUser(final DatabaseReference mDatabase, final ProgressBar progress, final TextView alertText, final String[] quotes, final String[] owners, final ViewPager viewPager, final SwipeAdapter swipeAdapter, final Context context, final int[] slider_images, final TextView share, final ImageView fav)
    {
        final boolean[] hasChildToday = {false};
        final boolean[] hasChildYesterday = {false};
        final String childCurrentDate = CustomDateForamtHelper.getInstance().getTodaysDate();
        final String childPrevDate = CustomDateForamtHelper.getInstance().returnDate("anotherday");
        final String currPeriod = CustomDateForamtHelper.getInstance().returnPeriodOfTime();

        int dateSelecter = returnDateAccordingToTime(currPeriod);

        if (dateSelecter == 1)
        {
            // for current date

            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot snapshot: dataSnapshot.getChildren())
                    {
                        if (snapshot.getKey().toString().equals(childCurrentDate))
                        {
                            hasChildToday[0] = true;
                            if (hasChildToday[0]) {
                                hideAlertTextView(alertText);
                                displayQuotes(mDatabase.child(childCurrentDate).getRef(), quotes, owners, viewPager, swipeAdapter, context, slider_images, share, progress, fav);
                                break;
                            }
                        }
                        else
                        {
                            showAlertText(progress, alertText, "Sorry, no quotes for today!");
                        }
                    }

//                    if (dataSnapshot.child(childCurrentDate).getRef() != null)
//                    {
//                        hasChildToday[0] = true;
//                        if(hasChildToday[0])
//                            displayQuotes(mDatabase.child(childCurrentDate).getRef(), quotes, owners, viewPager, swipeAdapter, context, slider_images, share, progress, fav);
//                        else
//                            showAlertText(progress, alertText, "Sorry, no quotes for today!");
//                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
        else if (dateSelecter == 2)
        {
            // for previous date

            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child(childPrevDate).getRef() != null)
                    {
                        hasChildYesterday[0] = true;
                        if (hasChildYesterday[0]) {
                            displayQuotes(mDatabase.child(childPrevDate).getRef(), quotes, owners, viewPager, swipeAdapter, context, slider_images, share, progress, fav);
                            hideAlertTextView(alertText);
                        }
                        else
                            showAlertText(progress, alertText, "Sorry, no quotes for yesterday!");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


    }

    private void displayQuotes(DatabaseReference mDatabase, final String[] quotes, final String[] owners, final ViewPager viewPager, final SwipeAdapter swipeAdapter, final Context context, final int[] slider_images, final TextView share, final ProgressBar progress, final ImageView fav) // to display quotes for previous date
    {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                setQuoteAndOwnerData(dataSnapshot.getRef(), quotes, owners, viewPager, swipeAdapter, context, slider_images, share, progress, fav);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setQuoteAndOwnerData(DatabaseReference reference, final String[] quotes, final String[] owners, final ViewPager viewPager, final SwipeAdapter swipeAdapter, final Context context, final int[] slider_images, final TextView share, final ProgressBar progress, final ImageView fav)
    {
        int i=0;
        if (reference != null)
        {
            reference.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    long size = dataSnapshot.getChildrenCount();
                    int i = 0;
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    {
                        owners[i] = snapshot.child("owner").getValue().toString();
                        quotes[i] = snapshot.child("text").getValue().toString();
                        i++;
                    }
                    if (i == (int) size)
                    {
                        setTheAdapter(viewPager, swipeAdapter, context, slider_images, quotes, owners);
                        share.setEnabled(true);
                        progress.setVisibility(View.INVISIBLE);
                        fav.setEnabled(true);
                    }
                    if (checkIfDataAlreadyExists(quotes[0])) { fav.setImageResource(R.drawable.heart); }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) { }
            });
        }
    }

    private boolean checkIfDataAlreadyExists(String text)
    {
        List<User> users = MorningQuotes.userDatabase.daoAccess().getUsers();

        boolean hasValue = false;

        for (int i=0; i<users.size(); i++)
        {
            User user = users.get(i);
            if (user.getQuotes().equals(text))
            {
                hasValue = true;
                break;
            }
        }
        if (hasValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private int returnDateAccordingToTime(String currPeriod)
    {
        if (checkForEquals(currPeriod, "morning") || checkForEquals(currPeriod, "afternoon") || checkForEquals(currPeriod, "evening"))
            return 1;
        else if (checkForEquals(currPeriod, "anotherday"))
            return 2;
        else return 0;

    }

    private boolean checkForEquals(String a, String b)
    {
        return a.equals(b);
    }

    private void populateEmptyDataAndDisplayMessage(ViewPager viewPager, SwipeAdapter swipeAdapter, Context context, int[] slider_images, String[] quotes, String[] owners, ProgressBar progress, TextView alertText, String message, ImageView fav)
    {
        fav.setEnabled(false);
        initAndSetEmptyAdapter(viewPager, swipeAdapter, context, slider_images, quotes, owners);
        showAlertText(progress, alertText, message);
    }

    private void showAlertText(ProgressBar progress, TextView noInternet, String message)
    {
        progress.setVisibility(View.INVISIBLE);
        noInternet.setText(message);
        noInternet.setVisibility(View.VISIBLE);
    }

    private void initAndSetEmptyAdapter(ViewPager viewPager, SwipeAdapter swipeAdapter, Context context, int[] slider_images, String[] quotes, String[] owners)
    {
        quotes = initQuotes(quotes);
        owners = initOwners(owners);
        swipeAdapter = new SwipeAdapter(context, slider_images, quotes, owners);
        viewPager.setAdapter(swipeAdapter);
    }

    private void setTheAdapter(ViewPager viewPager, SwipeAdapter swipeAdapter, Context context, int[] slider_images, String[] quotes, String[] owners)
    {
        swipeAdapter = new SwipeAdapter(context, slider_images, quotes, owners);
        viewPager.setAdapter(swipeAdapter);
    }

    private String[] initQuotes(String[] quotes)
    {
        quotes[0] = quotes[1] = quotes[2] = "";
        return quotes;
    }

    private String[] initOwners(String[] owners)
    {
        owners[0] = owners[1] = owners[2] = "";
        return owners;
    }

    public void displayCategoryData(String category)
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Quotes/Categories" + category);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                QuotesHelper helper = dataSnapshot.getValue(QuotesHelper.class);

                System.out.println(helper);
                Log.d("a", "");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        Log.d("dd",mDatabase.toString());
    }

    private void hideAlertTextView(TextView alertText)
    {
        alertText.setVisibility(View.INVISIBLE);
    }

}
