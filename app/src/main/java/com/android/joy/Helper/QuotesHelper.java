package com.android.joy.Helper;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class QuotesHelper
{
    @SerializedName("quotes")
    public Map<String, Quotes> quotes;

    public QuotesHelper() {}

    public QuotesHelper(Map<String, Quotes> quotes)
    {
        this.quotes = quotes;
    }

    public class Quotes
    {

        public String quote1, quote2, quote3;

        public Quotes() {}

        public Quotes(String quote1, String quote2, String quote3) {
            this.quote1 = quote1;
            this.quote2 = quote2;
            this.quote3 = quote3;
        }
    }

}
