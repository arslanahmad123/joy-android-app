package com.android.joy.Helper;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.joy.R;

public class DataHelper
{
    private static DataHelper instance;

    public static DataHelper getInstance()
    {
        if (instance == null)
        {
            instance = new DataHelper();
        }
        return instance;
    }

    public void initializeSpinners(Context context, Spinner days, Spinner time)
    {
        ArrayAdapter<CharSequence> dayAdapter = ArrayAdapter.createFromResource(context, R.array.days, android.R.layout.simple_spinner_dropdown_item);
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        days.setAdapter(dayAdapter);

        ArrayAdapter<CharSequence> timeAdapter = ArrayAdapter.createFromResource(context, R.array.times, android.R.layout.simple_spinner_dropdown_item);
        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        time.setAdapter(timeAdapter);
    }

    public void setDateText(TextView dateText, TextView todayText)
    {
        String period = CustomDateForamtHelper.getInstance().returnPeriodOfTime();
        switch (period)
        {
            case "morning":
            case "afternoon":
            case "evening":
                setText(dateText, todayText, CustomDateForamtHelper.getInstance().getTodaysDate(), "Today's");
                break;
            case "anotherday":
                setText(dateText, todayText, CustomDateForamtHelper.getInstance().getPreviousDate(), "Yesterday's");
                break;
        }
    }

    private void setText(TextView dateText, TextView todayText, String date, String text)
    {
        dateText.setText(date);
        todayText.setText(text);
    }
}
