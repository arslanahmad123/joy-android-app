package com.android.joy.Helper;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;

public class FacebookLoginHelper
{
    private AccessToken accessToken;

    private CallbackManager callbackManager;

    public FacebookLoginHelper(AccessToken accessToken, CallbackManager callbackManager)
    {
        this.accessToken = accessToken;
        this.callbackManager = callbackManager;
    }

    public AccessToken getAccessToken()
    {
        return accessToken;
    }

    public CallbackManager getCallbackManager()
    {
        return callbackManager;
    }
}
