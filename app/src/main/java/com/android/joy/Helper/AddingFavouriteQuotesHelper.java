package com.android.joy.Helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.joy.Activities.LoginActivity;
import com.android.joy.Activities.MorningQuotes;
import com.android.joy.R;
import com.android.joy.Room.User;
import com.facebook.login.Login;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddingFavouriteQuotesHelper
{
    Context context;

    String uid, uname, quote, owner;

    ImageView fav;

    public AddingFavouriteQuotesHelper(Context context, String uid, String uname, String quote, ImageView fav, String owner)
    {
        this.context = context;
        this.uid = uid;
        this.uname = uname;
        this.quote = quote;
        this.fav = fav;
        this.owner = owner;
    }

    // the quotes addition comprises of these three functions ...

    public void addQuoteToFavourite()
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase = mDatabase.child("Users");
        if (mDatabase != null) // means there are users ..
        {
            // now, loop through each user and compare the id with uid and break when id equals to uid
            mDatabase.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot snapshot: dataSnapshot.getChildren())
                    {
                        String userId = snapshot.child("id").getValue().toString();
                        if (userId.equals(uid))
                        {
                            addQuoteFurtherProcessing(snapshot.getRef(), uid, uname, quote, owner);
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });
        }
    }

    public void addQuoteFurtherProcessing(final DatabaseReference ref, final String uid, final String uname, final String quote, final String onwer)
    {
        ref.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if (!dataSnapshot.hasChild("fav")) // means there is no favourite quote present in the user
                {
                    // now copy and pasting the logic from previous function
                    final Map<String, Object> hashMap = new HashMap<>();
                    hashMap.put("id", uid);
                    hashMap.put("name", uname);
                    hashMap.put("fav", "");

                    ref.updateChildren(hashMap);

                    Map<String, Object> hashmap = new HashMap<>();
                    hashmap.put("Quote", "");

                    DatabaseReference temp = ref.child("fav").getRef();
                    temp.push().updateChildren(hashmap);

                    temp.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            // now, we get child of temp 'fav'

                            for (DataSnapshot snapshot: dataSnapshot.getChildren()) {


                                DatabaseReference child = snapshot.child("Quote").getRef();

                                HashMap<String, Object> dataAdd = new HashMap<>();

                                dataAdd.put("text", quote);
                                dataAdd.put("owner", owner);

                                child.setValue(dataAdd);

                                HashMap<String, String> hashMapTypeString = new HashMap<>();
                                hashMapTypeString.put("text", quote);
                                hashMapTypeString.put("owner", owner);

                                ArrayList<Map<String, String>> hashMaps = new ArrayList<>();
                                hashMaps.add(hashMapTypeString);

                                storeDataToRoom(hashMaps);
                                break;
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                else // means there is a child which is 'fav'
                {
                    // now, we have to check if there are more than one favourite quotes or not ..
                    // now loop through to find the child of favourite ..
                    dataSnapshot.child("fav").getRef().addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            // loop through to find the child of 'fav' of the current user ..
                            for (DataSnapshot snapshot: dataSnapshot.getChildren())
                            {
                                addQuoteFurtherProcessingPartTwo(snapshot.getRef(), quote, owner);
                                break;
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError)
                        {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    public void addQuoteFurtherProcessingPartTwo(DatabaseReference ref, final String quote, final String owner)
    {
        // now, i have refrence of the child of 'fav'
        // now, i can check wether this child has one or more childs
        DatabaseReference mDatabase = ref.child("Quote");
        // now, checking if 'Quote has zero child then it means there are only one child which is quote otherwise more than one child'
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                // datasnapshot is 'Quote' here ..
                if (dataSnapshot.hasChild("owner")) // means only one child ..
                {
                    // now put the logic of one quote here ..
                    ArrayList<Map<String, String>> hashMaps = new ArrayList<>();

                    String valueOfSingleQuote = dataSnapshot.child("text").getValue().toString();
                    String prevOwner = dataSnapshot.child("owner").getValue().toString();

                    Map<String, String> mapPrev = new HashMap<>();

                    mapPrev.put("text", valueOfSingleQuote);
                    mapPrev.put("owner", prevOwner);

                    hashMaps.add(mapPrev);

                    Map<String, String> mapCurr = new HashMap<>();
                    mapCurr.put("text", quote);
                    mapCurr.put("owner", owner);

                    hashMaps.add(mapCurr);

                    // now, adding all quotes to database ..
                    dataSnapshot.getRef().setValue(hashMaps);

                    storeDataToRoom(hashMaps);
                }
                else // means, more than one child
                {
                    if (dataSnapshot.getChildrenCount() < 20)
                    {
                        ArrayList<Map<String, String>> hashMaps = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren())
                        {
                            Map<String, String> mapPrev = new HashMap<>();

                            String valueOfPrevQuote = snapshot.child("text").getValue().toString();
                            String owner = snapshot.child("owner").getValue().toString();

                            mapPrev.put("text", valueOfPrevQuote);
                            mapPrev.put("owner", owner);

                            hashMaps.add(mapPrev);
                        }

                        Map<String, String> currMap = new HashMap<>();
                        currMap.put("text", quote);
                        currMap.put("owner", owner);
                        hashMaps.add(currMap);

                        dataSnapshot.getRef().setValue(hashMaps); // now, adding all quotes including new one to database ..

                        storeDataToRoom(hashMaps); // storing data to sqlite
                    }
                    else
                    {
                        Toast.makeText(context, "Favourite Quotes limit full !", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    // end of functions of adding quotes to database ...


    private void storeDataToRoom(ArrayList<Map<String, String>> maps)
    {
        int size = maps.size();

        ArrayList<User> users = new ArrayList<>();

        for (int i=0; i<size; i++)
        {
            User user = new User();

            Map<String, String> map = maps.get(i);

            String value = map.get("text");
            String owner = map.get("owner");

            user.setUid(i);
            user.setQuotes(value);
            user.setOwners(owner);

            users.add(user);
        }

        fav.setImageResource(R.drawable.heart);
        LoginActivity.userDatabase.daoAccess().deleteAll();
        LoginActivity.userDatabase.daoAccess().insertAll(users);
        Log.d("Added", "Data Added");

    }


    // sending data of favourite quotes to local storage (Room)



    // start of adding favourite quotes and their owners to local storage

    public void storeFavouriteQuotesToLocalStorage()
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase = mDatabase.child("Users");
        if (mDatabase != null) // means there are users ..
        {
            // now, loop through each user and compare the id with uid and break when id equals to uid
            mDatabase.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    for (DataSnapshot snapshot: dataSnapshot.getChildren())
                    {
                        String userId = snapshot.child("id").getValue().toString();
                        if (userId.equals(uid))
                        {
                            addQuoteFurtherProcessing(snapshot.getRef(), uid);
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });
        }
        //MorningQuotes.userDatabase.daoAccess().deleteAll();
        //MorningQuotes.userDatabase.daoAccess().insertAll(users);
    }

    private void addQuoteFurtherProcessing(final DatabaseReference ref, final String uid)
    {
        // ref is specific user ..

        ref.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if (!dataSnapshot.hasChild("fav")) // means there is no favourite quote present in the user
                {
                    // no data to be added to room
                }
                else // means there is a child which is 'fav'
                {
                    // now, we have to check if there are more than one favourite quotes or not ..
                    // now loop through to find the child of favourite ..
                    dataSnapshot.child("fav").getRef().addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            // loop through to find the child of 'fav' of the current user ..
                            for (DataSnapshot snapshot: dataSnapshot.getChildren())
                            {
                                addQuoteFurtherProcessingPartTwo(snapshot.getRef());
                                break;
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError)
                        {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    private void addQuoteFurtherProcessingPartTwo(DatabaseReference ref)
    {
        // now, i have refrence of the child of 'fav'
        // now, i can check wether this child has one or more childs
        DatabaseReference mDatabase = ref.child("Quote");
        // now, checking if 'Quote has zero child then it means there are only one child which is quote otherwise more than one child'
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                // datasnapshot is 'Quote' here ..
                if (dataSnapshot.hasChild("owner")) // means only one child ..
                {
                    // now put the logic of adding one quote here ..
                    ArrayList<Map<String, String>> hashMaps = new ArrayList<>();

                    String valueOfSingleQuote = dataSnapshot.child("text").getValue().toString();
                    String owner = dataSnapshot.child("owner").getValue().toString();

                    Map<String, String> mapPrev = new HashMap<>();

                    mapPrev.put("text", valueOfSingleQuote);
                    mapPrev.put("owner", owner);

                    hashMaps.add(mapPrev);

                    ArrayList<User> users = new ArrayList<>();

                    User user = new User();
                    user.setUid(0);
                    user.setQuotes(valueOfSingleQuote);
                    user.setOwners(owner);

                    users.add(user);

                    // now, add data to room

                    List<User> list = LoginActivity.userDatabase.daoAccess().getUsers();

                    if (list != null)
                    {
                        for (int i = 0; i < list.size(); i++)
                        {
                            LoginActivity.userDatabase.daoAccess().delete(list.get(i));
                        }
                    }

                    MorningQuotes.userDatabase.daoAccess().insertAll(users);
                }
                else // means, more than one child
                {
                    ArrayList<User> users = new ArrayList<>();
                    int i=0;
                    for (DataSnapshot snapshot: dataSnapshot.getChildren())
                    {
                        String valueOfPrevQuote = snapshot.child("text").getValue().toString();
                        String owner = snapshot.child("owner").getValue().toString();

                        User user = new User();

                        user.setUid(i);
                        user.setQuotes(valueOfPrevQuote);
                        user.setOwners(owner);

                        users.add(user);

                        i++;
                    }
                    // now, add data to room


                    List<User> users1 = LoginActivity.userDatabase.daoAccess().getAll();

                    for (int j=0; j<users1.size(); j++)
                    {
                        LoginActivity.userDatabase.daoAccess().delete(users1.get(j));
                    }

                    LoginActivity.userDatabase.daoAccess().insertAll(users);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }


    // end of adding favourite quotes





}
